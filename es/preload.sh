#!/bin/sh

echo "Bundling template"
curl -s -XPUT -H 'Content-Type: application/json' http://localhost:9200/_index_template/hestia-data -d @/template.json

echo "Indexing terms"

# create index
curl -s -XPUT -H 'Content-Type: application/json' http://localhost:9200/hestia-data

# index all Term
for file in /Term/*
do
  if [[ -f $file ]]; then
    curl -s -XPOST -H 'Content-Type: application/json' http://localhost:9200/hestia-data/_doc -d "@$file"
  fi
done
