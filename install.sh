#!/bin/sh
#

ENV=".env"

download_glossary() {
  echo -n "Downloading the Lookups..."
  rm -rf glossary
  ./scripts/download-lookups.sh
  echo "done"

  echo -n "Downloading the Terms..."
  rm -rf Term
  ./scripts/download-terms.sh
  echo "done"

  echo -n "Creating environment file..."
  rm -rf $ENV
  echo "DOWNLOAD_FOLDER=${PWD}" >> $ENV
  echo "DOWNLOAD_FOLDER_GLOSSARY=${PWD}" >> $ENV
  echo "done"
}

download_config() {
  echo -n "Downloading the configuration files..."
  rm -rf config
  ./scripts/download-configs.sh
  echo "done"

  # remove models that cannot be used locally
  python3 scripts/remove_aggregated_model.py

  if [ "$1" = "true" ];
  then
    mkdir -p ${PWD}/ee
    echo "Please save your EE credentials under ${PWD}/ee/credentials.json"
    echo "MAX_AREA_SIZE=100000"
    echo "EARTH_ENGINE_KEY_FILE=${PWD}/ee/credentials.json"
  fi;
}

download_distribution() {
  echo -n "Downloading the distribution files..."
  rm -rf distribution
  ./scripts/download-distribution-files.sh
  echo "done"

  echo -n "Creating environment file..."
  rm -rf $ENV
  echo "DISTRIBUTION_DATA_FOLDER=${PWD}/distribution" >> $ENV
  echo "done"
}

read -p "Do you want to download the Glossary Terms and Lookups locally? [y/n] " yn
case $yn in
  [Yy]* ) download_glossary ;;
  [Nn]* ) ;;
  * ) echo "Please answer yes or no.";;
esac

# creating dependencies
DEPS="requirements.txt"
rm -rf $DEPS
cp requirements-main.txt $DEPS

read -p "Do you want to use and configure the Hestia Earth Engine library? [y/n] " yn
case $yn in
  [Yy]* ) download_config "true" ;;
  [Nn]* ) download_config "false" ;;
  * ) echo "Please answer yes or no.";;
esac

# download_distribution

echo "Installing dependencies"

pip3 install -r $DEPS

echo "Installation is finished. You can start running calculations using:"
echo "  python3 run.py samples/chicken.jsonld"
