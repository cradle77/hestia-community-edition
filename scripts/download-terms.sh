#!/bin/bash

set -e

DEST_DIR="Term"
API_URL=${1:-"https://api.hestia.earth"}

mkdir -p $DEST_DIR

# Download required files
declare -a termTypes=(
  "animalProduct"
  "animalManagement"
  "aquacultureManagement"
  "building"
  "characterisedIndicator"
  "crop"
  "cropResidue"
  "cropResidueManagement"
  "electricity"
  "emission"
  "endpointIndicator"
  "excreta"
  "excretaManagement"
  "fuel"
  "inorganicFertiliser"
  "irrigation"
  "landUseManagement"
  "liveAnimal"
  "liveAquaticSpecies"
  "machinery"
  "material"
  "measurement"
  "methodEmissionResourceUse"
  "methodMeasurement"
  "model"
  "operation"
  "organicFertiliser"
  "other"
  "pesticideAI"
  "pesticideBrandName"
  "property"
  "region"
  "resourceUse"
  "soilAmendment"
  "soilTexture"
  "soilType"
  "standardsLabels"
  "system"
  "tillage"
  "transport"
  "usdaSoilType"
  "water"
  "waterRegime"
)
for termType in "${termTypes[@]}"
do
  RES=$(curl -s -X GET "$API_URL/terms/backups/${termType}" -H "Content-Type: application/json")
  URL=$(jq -rc '.url' <<< "${RES}")
  ZIP_FILE="$TYPE.zip"
  curl -s -L "${URL}" -o $ZIP_FILE
  unzip -qq $ZIP_FILE -d .
  rm -f $ZIP_FILE
done
