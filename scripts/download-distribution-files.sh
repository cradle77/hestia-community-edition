#!/bin/bash

set -e

API_URL=${1:-"https://api.hestia.earth"}

DEST_DIR="distribution"
mkdir -p $DEST_DIR

RES=$(curl -s -X GET "$API_URL/distribution/files" -H "Content-Type: application/json")

PRIOR_YIELD_URL=$(jq -rc '.priorYield' <<< "${RES}")
mkdir -p "${DEST_DIR}/prior_files"
curl -s -L "${PRIOR_YIELD_URL}" -o "${DEST_DIR}/prior_files/FAO_Yield_prior_per_product_per_country.csv"

POST_YIELD_URL=$(jq -rc '.posteriorYield' <<< "${RES}")
mkdir -p "${DEST_DIR}/posterior_files"
curl -s -L "${POST_YIELD_URL}" -o "${DEST_DIR}/posterior_files/posterior_crop_yield.csv"
