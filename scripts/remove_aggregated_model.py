import json

IGNORE_MODELS = [
    'input.aggregated',
    'input.hestiaAggregatedData'
]
filepath = 'config/Cycle.json'

with open(filepath, 'r') as f:
    config = json.load(f)


def clean_model(model):
    return (not isinstance(model, dict)) or (model.get('value') not in IGNORE_MODELS)


config['models'] = list(filter(clean_model, config['models']))


data = json.dumps(config, indent=2, ensure_ascii=False)
with open(filepath, 'w') as f:
    f.write(data)
