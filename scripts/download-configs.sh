#!/bin/sh

set -e

BRANCH=${1:-"master"}

if [ "$BRANCH" = "develop" ]
then
  SNIPPET_ID="2106345"
else
  SNIPPET_ID="2106344"
fi

SNIPPET_URL="https://gitlab.com/hestia-earth/hestia-engine-orchestrator/-/snippets/${SNIPPET_ID}"
echo $SNIPPET_URL

DEST_DIR="config"
mkdir -p $DEST_DIR

curl -s "${SNIPPET_URL}/raw/master/Site.json?inline=false" -o "${DEST_DIR}/Site.json"
curl -s "${SNIPPET_URL}/raw/master/ImpactAssessment.json?inline=false" -o "${DEST_DIR}/ImpactAssessment.json"
curl -s "${SNIPPET_URL}/raw/master/Cycle.json?inline=false" -o "${DEST_DIR}/Cycle.json"
