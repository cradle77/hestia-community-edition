const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const [suffix, date] = process.argv.slice(2);
const versionSuffix = suffix ? `-${suffix}` : '';
const versionDate = date ? `-${date.replace(/[:-a-zA-Z]/g, '')}` : '';

const ROOT = resolve(join(__dirname, '../'));
const version = `${require(join(ROOT, 'package.json')).version}${versionSuffix}${versionDate}`;

// api
const PYTHON_VERSION_PATH = resolve(join(ROOT, 'api', 'app', 'version.py'));
let content = readFileSync(PYTHON_VERSION_PATH, 'UTF-8');
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(PYTHON_VERSION_PATH, content, 'UTF-8');

// ui
const JS_VERSION_PATH = resolve(join(ROOT, 'ui', 'package.json'));
content = require(JS_VERSION_PATH);
content.version = `${version}`;
writeFileSync(JS_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
const JS_LOCK_VERSION_PATH = resolve(join(ROOT, 'ui', 'package-lock.json'));
content = require(JS_LOCK_VERSION_PATH);
content.version = `${version}`;
writeFileSync(JS_LOCK_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
const ENV_PROD_PATH = resolve(join(ROOT, 'ui', 'src', 'environments', 'environment.prod.ts'));
content = readFileSync(ENV_PROD_PATH, 'UTF-8');
content = content.replace(/version: \'(.*)\'/, `version: '${version}'`);
writeFileSync(ENV_PROD_PATH, content, 'UTF-8');
const ENV_PATH = resolve(join(ROOT, 'ui', 'src', 'environments', 'environment.ts'));
content = readFileSync(ENV_PATH, 'UTF-8');
content = content.replace(/version: \'(.*)\'/, `version: '${version}'`);
writeFileSync(ENV_PATH, content, 'UTF-8');
