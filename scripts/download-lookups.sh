#!/bin/bash

set -e

BRANCH=${1:-"master"}
URL="https://gitlab.com/api/v4/projects/23686745"
TOKEN="glpat-nQCcuyPRTWsausyaJTNb"
LOOKUP_DIR="lookups"
DEST_DIR="glossary"

mkdir -p "${DEST_DIR}/${LOOKUP_DIR}"

download_lookups() {
  PAGE=${1:-"1"}
  PER_PAGE=${2:-"100"}

  RES=$(curl -H "PRIVATE-TOKEN: ${TOKEN}" -X GET "${URL}/repository/tree?per_page=${PER_PAGE}&page=${PAGE}&path=${LOOKUP_DIR}&ref=${BRANCH}")

  items=$(echo "$RES" | jq -c -r '.[]')
  for item in ${items[@]}; do
    id=$(echo $item | jq -r '.id')
    name=$(echo $item | jq -r '.name')
    curl -H "PRIVATE-TOKEN: ${TOKEN}" -X GET "${URL}/repository/blobs/${id}/raw" -o "./${DEST_DIR}/${LOOKUP_DIR}/${name}"
  done
}

download_lookups "1"
download_lookups "2"
