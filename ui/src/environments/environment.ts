import { IEnv } from './environment.interface';

export const environment: IEnv = {
  production: false,
  version: '2.6.2',
  apiUrl: 'http://localhost:3000',
  orchestratorConfigUrl: 'https://gitlab.com/hestia-earth/hestia-engine-orchestrator/-/snippets/2106345/raw/master'
};
