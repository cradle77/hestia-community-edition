export interface IEnv {
  production: boolean;
  version: string;
  apiUrl: string;
  orchestratorConfigUrl: string;
}
