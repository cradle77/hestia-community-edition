import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { HE_API_BASE_URL, HE_CALCULATIONS_BASE_URL, HE_ORCHESTRATOR_BASE_URL } from '@hestia-earth/ui-components';

import { environment } from '../environments/environment';
import { SharedModule } from './shared/shared.module';
import { ApiVersionInterceptor } from './api-version.interceptor';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { GeospatialSearchPageComponent } from './geospatial-search-page/geospatial-search-page.component';
import { TermsPageComponent } from './terms-page/terms-page.component';

@NgModule({
  declarations: [
    AppComponent,
    GeospatialSearchPageComponent,
    TermsPageComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
    NgxWebstorageModule.forRoot({
      prefix: 'he_ce',
      separator: '-'
    }),
    AppRoutingModule, FormsModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiVersionInterceptor,
      multi: true
    },
    {
      provide: HE_API_BASE_URL,
      useValue: environment.apiUrl
    },
    {
      provide: HE_CALCULATIONS_BASE_URL,
      useValue: environment.apiUrl
    },
    {
      provide: HE_ORCHESTRATOR_BASE_URL,
      useValue: environment.orchestratorConfigUrl
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
