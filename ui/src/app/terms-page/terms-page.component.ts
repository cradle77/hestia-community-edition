import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

import { storageKey, versionDate } from '../terms.model';

@Component({
  selector: 'app-terms-page',
  templateUrl: './terms-page.component.html',
  styleUrls: ['./terms-page.component.scss']
})
export class TermsPageComponent {
  public versionDate = versionDate;
  public year = new Date().getFullYear();

  constructor(
    private router: Router,
    private localStorage: LocalStorageService
  ) { }

  public accept() {
    this.localStorage.store(storageKey, versionDate);
    return this.router.navigate(['/']);
  }
}
