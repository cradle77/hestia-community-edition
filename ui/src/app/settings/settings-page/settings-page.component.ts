import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

import { ApiSettingsService, ISettings, DataFormat } from '../../api-settings.service';

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent implements OnInit {
  public DataFormat = DataFormat;
  public loading = true;
  public form?: UntypedFormGroup;
  public settings?: ISettings;
  public saving = false;
  public error?: string;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private settingsService: ApiSettingsService
  ) { }

  async ngOnInit() {
    this.settings = await this.settingsService.getAll();
    this.form = this.formBuilder.group({
      defaultFormat: [this.settings.defaultFormat]
    });
    this.loading = false;
  }

  public async save() {
    this.saving = true;
    this.error = undefined;
    try {
      await this.settingsService.updateAll(this.form?.value);
    }
    catch (err: any) {
      console.error(err);
      this.error = err.stack || err.message;
    }
    finally {
      this.saving = false;
    }
  }
}
