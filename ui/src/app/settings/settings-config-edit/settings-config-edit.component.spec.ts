import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigEditComponent } from './settings-config-edit.component';

describe('SettingsConfigEditComponent', () => {
  let component: SettingsConfigEditComponent;
  let fixture: ComponentFixture<SettingsConfigEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
