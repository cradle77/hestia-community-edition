import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { JsonEditorOptions } from 'ang-jsoneditor';
import { create, formatters } from 'jsondiffpatch';
import { IOrchestratorConfig } from '@hestia-earth/ui-components/engine/engine.service';

const diff = create();

@Component({
  selector: 'app-settings-config-edit',
  templateUrl: './settings-config-edit.component.html',
  styleUrls: ['./settings-config-edit.component.scss']
})
export class SettingsConfigEditComponent implements OnChanges {
  @Input()
  private oldConfig?: IOrchestratorConfig;
  @Input()
  public newConfig?: IOrchestratorConfig;
  @Input()
  public editable = true;

  @Output()
  private configChanged = new EventEmitter<IOrchestratorConfig>();

  public editorOptions = new JsonEditorOptions();
  public diffHtml?: string;

  ngOnChanges() {
    this.editorOptions.mode = this.editable ? 'code' : 'text';
    const delta = diff.diff(this.oldConfig, this.newConfig);
    const diffs = formatters.html.format(delta!, this.oldConfig);
    this.diffHtml = diffs;
  }

  public reloadDiffs() {
    this.configChanged.emit(this.newConfig);
  }
}
