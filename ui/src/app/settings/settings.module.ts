import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { HeCommonModule, HeEngineModule } from '@hestia-earth/ui-components';

import { SharedModule } from '../shared/shared.module';
import { SettingsRoutingModule } from './settings-routing.module';

import { SettingsConfigListPageComponent } from './settings-config-list-page/settings-config-list-page.component';
import { SettingsConfigEditPageComponent } from './settings-config-edit-page/settings-config-edit-page.component';
import { SettingsConfigEditComponent } from './settings-config-edit/settings-config-edit.component';
import { SettingsConfigNewConfirmComponent } from './settings-config-new-confirm/settings-config-new-confirm.component';
import { SettingsConfigDeleteConfirmComponent } from './settings-config-delete-confirm/settings-config-delete-confirm.component';
import { SettingsPageComponent } from './settings-page/settings-page.component';
import { SettingsConfigShowPageComponent } from './settings-config-show-page/settings-config-show-page.component';

@NgModule({
  declarations: [
    SettingsConfigListPageComponent,
    SettingsConfigEditPageComponent,
    SettingsConfigEditComponent,
    SettingsConfigNewConfirmComponent,
    SettingsConfigDeleteConfirmComponent,
    SettingsPageComponent,
    SettingsConfigShowPageComponent
  ],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    NgJsonEditorModule,
    HeCommonModule, HeEngineModule,
    SharedModule,
    SettingsRoutingModule
  ]
})
export class SettingsModule { }
