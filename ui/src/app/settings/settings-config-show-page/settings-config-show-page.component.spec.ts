import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigShowPageComponent } from './settings-config-show-page.component';

describe('SettingsConfigShowPageComponent', () => {
  let component: SettingsConfigShowPageComponent;
  let fixture: ComponentFixture<SettingsConfigShowPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigShowPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigShowPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
