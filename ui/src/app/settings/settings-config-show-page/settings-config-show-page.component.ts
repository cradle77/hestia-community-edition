import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { ApiConfigService, IConfigs, defaultConfigId } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-show-page',
  templateUrl: './settings-config-show-page.component.html',
  styleUrls: ['./settings-config-show-page.component.scss']
})
export class SettingsConfigShowPageComponent implements OnInit {
  public loading = true;
  public id?: string;
  public configs?: IConfigs;
  public configClosed: any = {};
  public saving = false;
  public showDelete = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configService: ApiConfigService
  ) {}

  async ngOnInit() {
    const { id } = await this.route.params.pipe(take(1)).toPromise();
    this.id = id;
    this.loading = true;
    this.configs = await this.configService.getConfigs(this.id!);
    this.loading = false;
  }

  public get canEdit() {
    return this.id !== defaultConfigId;
  }

  public deleteConfirmed(confirmed: boolean) {
    this.showDelete = false;
    return confirmed ? this.router.navigate(['/settings']) : null;
  }
}
