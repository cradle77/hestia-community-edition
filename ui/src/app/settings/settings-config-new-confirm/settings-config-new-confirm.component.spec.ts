import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigNewConfirmComponent } from './settings-config-new-confirm.component';

describe('SettingsConfigNewConfirmComponent', () => {
  let component: SettingsConfigNewConfirmComponent;
  let fixture: ComponentFixture<SettingsConfigNewConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigNewConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigNewConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
