import { Component, EventEmitter, Output } from '@angular/core';

import { ApiConfigService } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-new-confirm',
  templateUrl: './settings-config-new-confirm.component.html',
  styleUrls: ['./settings-config-new-confirm.component.scss']
})
export class SettingsConfigNewConfirmComponent {
  @Output()
  public closed = new EventEmitter<string>();

  public loading = false;
  public id = '';
  public submitted = false;

  constructor(
    private configService: ApiConfigService
  ) { }

  public get formValid() {
    return this.id !== 'default';
  }

  public async confirm() {
    this.submitted = true;
    if (!this.id || !this.formValid) {
      return;
    }

    this.loading = true;
    await this.configService.createConfig(this.id);
    this.closed.next(this.id);
  }
}
