import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ApiConfigService } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-delete-confirm',
  templateUrl: './settings-config-delete-confirm.component.html',
  styleUrls: ['./settings-config-delete-confirm.component.scss']
})
export class SettingsConfigDeleteConfirmComponent {
  @Input()
  public id = '';
  @Output()
  public closed = new EventEmitter<boolean>();

  public loading = false;

  constructor(
    private configService: ApiConfigService
  ) { }

  public async confirm() {
    this.loading = true;
    await this.configService.deleteConfig(this.id);
    this.closed.next(true);
  }
}
