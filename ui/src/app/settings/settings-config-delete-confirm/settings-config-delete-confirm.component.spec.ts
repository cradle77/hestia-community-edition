import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigDeleteConfirmComponent } from './settings-config-delete-confirm.component';

describe('SettingsConfigDeleteConfirmComponent', () => {
  let component: SettingsConfigDeleteConfirmComponent;
  let fixture: ComponentFixture<SettingsConfigDeleteConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigDeleteConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
