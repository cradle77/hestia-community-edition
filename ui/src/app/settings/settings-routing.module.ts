import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsPageComponent } from './settings-page/settings-page.component';
import { SettingsConfigListPageComponent } from './settings-config-list-page/settings-config-list-page.component';
import { SettingsConfigEditPageComponent } from './settings-config-edit-page/settings-config-edit-page.component';
import { SettingsConfigShowPageComponent } from './settings-config-show-page/settings-config-show-page.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsPageComponent
  },
  {
    path: 'config',
    component: SettingsConfigListPageComponent
  },
  {
    path: 'config/:id',
    component: SettingsConfigShowPageComponent
  },
  {
    path: 'config/:id/edit',
    component: SettingsConfigEditPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
