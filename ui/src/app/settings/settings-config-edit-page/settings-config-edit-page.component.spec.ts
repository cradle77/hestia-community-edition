import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigEditPageComponent } from './settings-config-edit-page.component';

describe('SettingsConfigEditPageComponent', () => {
  let component: SettingsConfigEditPageComponent;
  let fixture: ComponentFixture<SettingsConfigEditPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigEditPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
