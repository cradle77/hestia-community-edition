import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { NodeType } from '@hestia-earth/schema';
import { IOrchestratorConfig } from '@hestia-earth/ui-components/engine/engine.service';

import { ApiConfigService, IConfigs, defaultConfigId } from '../../api-config.service';

const copy = (data: any) => JSON.parse(JSON.stringify(data));
const isEqual = (a1: any, a2: any) => JSON.stringify(a1) === JSON.stringify(a2);

@Component({
  selector: 'app-settings-config-edit-page',
  templateUrl: './settings-config-edit-page.component.html',
  styleUrls: ['./settings-config-edit-page.component.scss']
})
export class SettingsConfigEditPageComponent implements OnInit {
  public loading = true;
  public id?: string;
  public configs?: IConfigs;
  public configClosed: any = {};
  public saving = false;
  public showDelete = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configService: ApiConfigService
  ) {}

  async ngOnInit() {
    const { id } = await this.route.params.pipe(take(1)).toPromise();
    this.id = id;
    this.loading = true;
    this.configs = await this.configService.getConfigs(this.id!);
    this.loading = false;
  }

  public get canEdit() {
    return this.id !== defaultConfigId;
  }

  public setConfig(config: IOrchestratorConfig, type: NodeType) {
    (this.configs as any)[type].config = config;
  }

  public hasOverride(type: NodeType) {
    return !isEqual((this.configs as any)[type].default, (this.configs as any)[type].config);
  }

  public async reset(type: NodeType) {
    (this.configs as any)[type].config = copy((this.configs as any)[type].default);
  }

  public async update() {
    this.saving = true;
    await Promise.all(Object.entries(this.configs!).map(([type, value]) =>
      this.configService.updateConfig(this.id!, type as NodeType, value.config)
    ));
    this.saving = false;
  }

  public deleteConfirmed(confirmed: boolean) {
    this.showDelete = false;
    return confirmed ? this.router.navigate(['/settings']) : null;
  }
}
