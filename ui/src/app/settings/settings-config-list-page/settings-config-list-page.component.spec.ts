import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsConfigListPageComponent } from './settings-config-list-page.component';

describe('SettingsConfigListPageComponent', () => {
  let component: SettingsConfigListPageComponent;
  let fixture: ComponentFixture<SettingsConfigListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsConfigListPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsConfigListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
