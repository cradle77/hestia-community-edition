import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { scrollToEl, filterParams } from '@hestia-earth/ui-components';

import { sortByFields, sortByOrder } from '../../api.service';
import { ApiConfigService, ConfigListResult, IConfigListResult, defaultConfigId } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-list-page',
  templateUrl: './settings-config-list-page.component.html',
  styleUrls: ['./settings-config-list-page.component.scss']
})
export class SettingsConfigListPageComponent implements OnInit {
  public loading = true;
  public defaultConfigId = defaultConfigId;

  public search = '';
  public perPage = 10;
  public page = 1;
  public showSortBy = false;
  public sortBy: sortByFields = 'createdOn';
  public sortOrder: sortByOrder = 'desc';
  public results = new ConfigListResult();

  public showNew = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configService: ApiConfigService
  ) { }

  ngOnInit() {
    const params = this.route.snapshot.queryParamMap;
    this.search = params.get('query') || '';
    this.sortBy = params.get('sortBy') as sortByFields || 'createdOn';
    this.sortOrder = params.get('sortOrder') as sortByOrder || 'desc';
    this.page = +(params.get('page') || '1');
    return this.filter();
  }

  public scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  public trackByResult(_index: number, { id }: IConfigListResult) {
    return id;
  }

  sort(sortBy: sortByFields) {
    this.sortOrder = this.sortBy === sortBy ? (
      this.sortOrder === 'asc' ? 'desc' : 'asc'
    ) : 'desc';
    this.sortBy = sortBy;
    return this.runSearch();
  }

  private async updateQueryParams() {
    const queryParams = this.queryParams;
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      ...(Object.keys(queryParams).length ? { queryParams } : {})
    });
  }

  public async runSearch() {
    this.page = 1;
    return this.filter();
  }

  public async filter() {
    await this.updateQueryParams();
    this.loading = true;
    this.results = await this.configService.listConfigs(this.searchParams);
    this.loading = false;
  }

  public get queryParams() {
    return filterParams({
      query: this.search,
      page: this.page,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    });
  }

  private get searchParams() {
    return {
      search: this.search,
      limit: this.perPage,
      offset: this.offset,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    };
  }

  public get total() {
    return this.results?.count ?? 0;
  }

  public get offset() {
    return (this.page - 1) * this.perPage;
  }

  public createConfirmed(id: string) {
    this.showNew = false;
    return id ? this.router.navigate(['./', id, 'edit'], {
      relativeTo: this.route
    }) : null;
  }
}
