import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';

const path = `${environment.apiUrl}/settings`;

export enum DataFormat {
  Hestia = 'Hestia',
  PooreNemecek = 'Poore & Nemecek',
  OpenLCA = 'Open LCA'
}

export interface ISettings {
  defaultFormat?: DataFormat;
}

@Injectable({
  providedIn: 'root'
})
export class ApiSettingsService {
  constructor(
    private http: HttpClient
  ) { }

  public getAll() {
    return this.http.get<ISettings>(path).toPromise().catch(() => ({ defaultFormat: DataFormat.Hestia }));
  }

  public updateAll(settings: ISettings) {
    return this.http.put<{ success: boolean }>(path, settings).toPromise();
  }

  public getSingle(key: keyof ISettings) {
    return this.http.get<any>(`${path}/${key}`).toPromise();
  }

  public updateSingle(key: keyof ISettings, value: any) {
    return this.http.put<{ success: boolean }>(`${path}/${key}`, value).toPromise();
  }
}
