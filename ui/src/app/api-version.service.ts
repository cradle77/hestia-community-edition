import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { gitHome } from '@hestia-earth/ui-components';

import { environment } from '../environments/environment';

export interface IVersion {
  current: string;
  latest: string;
  isLatest: boolean;
  isPrerelease: boolean;
  models: string;
  orchestrator: string;
  validation: string;
}

export interface IVersionData {
  version: string;
  isPrerelease: boolean;
  commitSha?: string;
  date?: string;
}

export const gitUrl = `${gitHome}/hestia-community-edition`;

export const versionUrl = ({ isPrerelease, version, commitSha }: IVersionData) =>
  isPrerelease
    ? `${gitUrl}/-/tree/v${version}`
    : commitSha
    ? `${gitUrl}/-/commit/${commitSha}`
    : `${gitUrl}/-/releases/v${version}`;

const parseDate = (date: string) => [
  date.substring(0, 4),
  date.substring(4, 6),
  date.substring(6, 8)
].join('-');

export const parseVersion = (version: string): IVersionData => {
  const parts = version.split('-');
  return parts.length === 2
    ? {
      version,
      isPrerelease: true
    }
    : parts.length === 3
    ? {
      version: parts[0],
      isPrerelease: false,
      commitSha: parts[1],
      date: parseDate(parts[2])
    }
    : {
      version,
      isPrerelease: false
    };
};

@Injectable({
  providedIn: 'root'
})
export class ApiVersionService {
  private _version = new ReplaySubject<string>(1);
  private _pandasVersion = new ReplaySubject<string>(1);

  constructor(
    private http: HttpClient
  ) { }

  public getVersions() {
    return this.http.get<IVersion>(`${environment.apiUrl}/version`).toPromise().catch(() => undefined);
  }

  public updateVersion(version: string) {
    this._version.next(version);
  }

  public get version$() {
    return this._version.asObservable();
  }

  public get versionData$() {
    return this.version$.pipe(map(parseVersion));
  }

  public updatePandasVersion(version: string) {
    this._pandasVersion.next(version);
  }

  public get pandasVersion$() {
    return this._pandasVersion.asObservable();
  }
}
