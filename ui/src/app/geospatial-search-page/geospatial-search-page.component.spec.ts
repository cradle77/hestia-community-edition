import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeospatialSearchPageComponent } from './geospatial-search-page.component';

describe('GeospatialSearchPageComponent', () => {
  let component: GeospatialSearchPageComponent;
  let fixture: ComponentFixture<GeospatialSearchPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeospatialSearchPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeospatialSearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
