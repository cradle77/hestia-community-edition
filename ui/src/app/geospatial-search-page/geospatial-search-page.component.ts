import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { arrayValue } from '@hestia-earth/utils/dist/term';
import { repeat, baseUrl } from '@hestia-earth/ui-components';

import { ApiService, ICollection, ICollectionExtended } from '../api.service';

const collections: ICollection[] = [
  {
    name: 'Soil pH',
    collection: 'users/hestiaplatform/T_PH_H2O',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Clay content',
    collection: 'users/hestiaplatform/T_CLAY',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Sand content',
    collection: 'users/hestiaplatform/T_SAND',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Organic carbon content',
    collection: 'users/hestiaplatform/T_OC',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Nitrogen content',
    collection: 'users/hestiaplatform/T_N',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Phosphorous content',
    collection: 'users/hestiaplatform/soil_phosphorus_concentration',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Slope length',
    collection: 'users/hestiaplatform/slope_length_factor',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Slope',
    collection: 'users/hestiaplatform/GMTEDSLOPE',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Drainage class',
    collection: 'users/hestiaplatform/drainage-xy1',
    ee_type: 'raster',
    reducer: 'mode',
    fields: 'mode'
  },
  {
    name: 'Nutrient loss aquatic',
    collection: 'users/hestiaplatform/r_fraction_loss_water',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Erodibility',
    collection: 'users/hestiaplatform/erodibility',
    ee_type: 'raster',
    reducer: 'mean',
    fields: 'mean'
  },
  {
    name: 'Annual rainfall',
    collection: 'ECMWF/ERA5/MONTHLY',
    ee_type: 'raster_by_period',
    reducer: 'sum',
    fields: 'sum',
    band_name: 'total_precipitation'
  },
  {
    name: 'Heavy winter precipitation',
    collection: 'users/hestiaplatform/correction_winter-type_precipitation',
    ee_type: 'raster',
    reducer: 'mode',
    fields: 'mode'
  },
  {
    name: 'Annual temperature',
    collection: 'ECMWF/ERA5/MONTHLY',
    ee_type: 'raster_by_period',
    reducer: 'mean',
    fields: 'mean',
    band_name: 'mean_2m_air_temperature'
  },
  {
    name: 'Eco-climate Zone',
    collection: 'users/hestiaplatform/climate_zone',
    ee_type: 'raster',
    reducer: 'mode',
    fields: 'mode'
  },
  {
    name: 'Ecoregion',
    collection: 'users/hestiaplatform/Terrestrial_Ecoregions_World',
    ee_type: 'vector',
    fields: 'eco_code'
  }
];

interface IResult {
  name: string;
  value: any;
}

const convertValueField: {
  [key: string]: (value: number) => number;
} = {
  'Clay content': v => v / 100,
  'Sand content': v => v / 100,
  'Organic carbon content': v => v / 100,
  Slope: v => v / 100,
  'Annual rainfall': v => v * 1000,
  'Annual temperature': v => v - 273.15
};

const convertValue = (properties: any, { name, fields }: ICollection) =>
  name in convertValueField ? convertValueField[name](properties[fields]) : properties[fields];

const getProperties = ({ features }: any) => features[0].properties;

const inclYears = 'ECMWF/ERA5/MONTHLY';

@Component({
  selector: 'app-geospatial-search-page',
  templateUrl: './geospatial-search-page.component.html',
  styleUrls: ['./geospatial-search-page.component.scss']
})
export class GeospatialSearchPageComponent {
  public baseUrl = baseUrl();
  public collections = collections;
  public selectedCollection = [...collections];
  public regionId?: string;
  public latitude?: number;
  public longitude?: string;
  public startYear?: number;
  public endYear = 2000;

  public submitted = false;
  public loading = false;
  public result?: IResult[];
  public csvContent?: SafeResourceUrl;
  public error?: any;

  constructor(
    private domSanitizer: DomSanitizer,
    private service: ApiService
  ) { }

  public get enableYear() {
    return (this.selectedCollection || []).map(({ collection }) => collection).includes(inclYears);
  }

  public get downloadFilename() {
    return `${this.regionId}-data.csv`;
  }

  private async collectionValue(collection: ICollectionExtended) {
    return convertValue(getProperties(await this.service.ee(this.regionId ? 'gadm' : 'coordinates', collection)), collection);
  }

  private extendCollection(collection: ICollection) {
    const diff = this.startYear ? Math.abs(this.endYear - this.startYear) : 0;
    const years = collection.collection === inclYears ? repeat(diff + 1).map((_, i) => this.endYear - i) : [];
    const regionOrLocation =
      !this.regionId ? {
        latitude: this.latitude,
        longitude: this.longitude
      }
      : { gadm_id: this.regionId };
    return {
      ...collection,
      ...regionOrLocation,
      max_area: 100000,
      years
    } as ICollectionExtended;
  }

  public get hasError() {
    return !this.regionId && (!this.latitude || !this.longitude);
  }

  public async submit() {
    this.submitted = true;
    if (this.hasError) {
      return;
    }

    this.loading = true;
    this.error = null;
    this.result = undefined;

    try {
      const collectionData = (this.selectedCollection || []).map(data => this.extendCollection(data));
      const results = await Promise.all(collectionData.map(async collection =>
        collection.years?.length ?
          {
            name: collection.name,
            value: arrayValue(await Promise.all(collection.years?.map(
              year => this.collectionValue({ ...collection, year })
            )), true)
          } :
          {
            name: collection.name,
            value: await this.collectionValue(collection)
          }
      ));
      this.result = results;

      const csvContent = [
        `Region ID,${results.map(r => r.name).join(',')}`,
        `${this.regionId},${results.map(r => r.value).join(',')}`
      ].join('\n');
      this.csvContent = this.domSanitizer.bypassSecurityTrustResourceUrl(
        `data:text/html;charset=utf-8,${encodeURIComponent(csvContent)}`
      );
    }
    catch (err: any) {
      console.error(err);
      this.error = `<span>There was an error:</span> <code>${err.message}</code>`;
    }
    finally {
      this.loading = false;
    }
  }
}
