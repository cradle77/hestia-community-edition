import { Component } from '@angular/core';

import { environment } from '../../../environments/environment';
import { parseVersion, versionUrl } from '../../api-version.service';

@Component({
  selector: 'app-ui-version',
  templateUrl: './ui-version.component.html',
  styleUrls: ['./ui-version.component.scss']
})
export class UiVersionComponent {
  public versionUrl = versionUrl;
  public version = parseVersion(environment.version);
}
