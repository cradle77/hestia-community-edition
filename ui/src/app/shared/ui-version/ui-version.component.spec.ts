import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiVersionComponent } from './ui-version.component';

describe('UiVersionComponent', () => {
  let component: UiVersionComponent;
  let fixture: ComponentFixture<UiVersionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiVersionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
