import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { gitHome, baseUrl, HeCommonService } from '@hestia-earth/ui-components';

const fixedTopBodyClass = 'has-spaced-navbar-fixed-top';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input()
  public isTransparent = false;

  public baseUrl = baseUrl();
  public gitHome = gitHome;
  public menuActive = false;

  constructor(
    private router: Router,
    public commonService: HeCommonService
  ) { }

  ngOnInit() {
    return document.body.classList.add(fixedTopBodyClass);
  }

  ngOnDestroy() {
    return document.body.classList.remove(fixedTopBodyClass);
  }

  isCurrentUrl(url: string) {
    return this.router.url === url;
  }

  startsWithUrl(url: string) {
    return this.router.url.startsWith(url);
  }
}
