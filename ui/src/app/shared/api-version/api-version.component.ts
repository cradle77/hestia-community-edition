import { Component } from '@angular/core';

import { ApiVersionService, versionUrl } from '../../api-version.service';

@Component({
  selector: 'app-api-version',
  templateUrl: './api-version.component.html',
  styleUrls: ['./api-version.component.scss']
})
export class ApiVersionComponent {
  public versionUrl = versionUrl;

  constructor(
    public apiVersionService: ApiVersionService
  ) { }
}
