import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { HeCommonModule } from '@hestia-earth/ui-components';

import { FontawesomeModule } from '../fontawesome/fontawesome.module';

import { ApiVersionComponent } from './api-version/api-version.component';
import { UiVersionComponent } from './ui-version/ui-version.component';
import { CheckNewVersionComponent } from './check-new-version/check-new-version.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TitleComponent } from './title/title.component';

const components = [
  ApiVersionComponent,
  UiVersionComponent,
  CheckNewVersionComponent,
  NavbarComponent,
  TitleComponent
];

@NgModule({
  declarations: components,
  exports: [
    NgbPaginationModule,
    HeCommonModule,
    FontawesomeModule,
    ...components
  ],
  imports: [
    CommonModule, RouterModule, FormsModule,
    NgbPaginationModule,
    HeCommonModule,
    FontawesomeModule
  ]
})
export class SharedModule { }
