import { Component, OnDestroy, HostBinding, Input, OnChanges } from '@angular/core';
import { Title } from '@angular/platform-browser';

const defaultTitle = 'Hestia CE';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnChanges, OnDestroy {
  @HostBinding('class')
  public classes = 'is-hidden';

  @Input()
  public title?: string;

  constructor(
    private titleService: Title
  ) { }

  ngOnChanges() {
    this.title && this.titleService.setTitle(`${this.title} | ${defaultTitle}`);
  }

  ngOnDestroy() {
    this.titleService.setTitle(defaultTitle);
  }
}
