import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckNewVersionComponent } from './check-new-version.component';

describe('CheckNewVersionComponent', () => {
  let component: CheckNewVersionComponent;
  let fixture: ComponentFixture<CheckNewVersionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckNewVersionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckNewVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
