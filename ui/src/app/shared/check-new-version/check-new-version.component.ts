import { Component, OnInit } from '@angular/core';

import { ApiVersionService, IVersion, gitUrl } from '../../api-version.service';

@Component({
  selector: 'app-check-new-version',
  templateUrl: './check-new-version.component.html',
  styleUrls: ['./check-new-version.component.scss']
})
export class CheckNewVersionComponent implements OnInit {
  public gitUrl = gitUrl;
  public version?: IVersion;
  public visible = false;

  constructor(
    private versionService: ApiVersionService
  ) { }

  async ngOnInit() {
    this.version = await this.versionService.getVersions();
    this.visible = !this.version?.isLatest && !this.version?.isPrerelease;
  }
}
