import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { filterParams } from '@hestia-earth/ui-components';
import { IOrchestratorConfig } from '@hestia-earth/ui-components/engine/engine.service';

import { environment } from '../environments/environment';
import { ISearchParams } from './api.service';

const path = `${environment.apiUrl}/config`;
export const defaultConfigId = 'default';

export interface IConfigListResult {
  id: string;
}

export class ConfigListResult {
  results: IConfigListResult[] = [];
  count = 0;
}

export interface IConfig {
  type: NodeType;
  hasOverride: boolean;
  updatedOn?: number;
  config: IOrchestratorConfig;
  default: IOrchestratorConfig;
}

export interface IConfigs {
  [NodeType.Cycle]: IConfig;
  [NodeType.ImpactAssessment]: IConfig;
  [NodeType.Site]: IConfig;
}

@Injectable({
  providedIn: 'root'
})
export class ApiConfigService {
  constructor(
    private http: HttpClient
  ) { }

  public listConfigs(params: ISearchParams = {}) {
    return this.http.get<ConfigListResult>(path, {
      params: filterParams(params)
    }).toPromise();
  }

  public getConfigs(id: string) {
    return this.http.get<IConfigs>(`${path}/${id}`).toPromise();
  }

  public createConfig(id: string) {
    return this.http.post<{ success: boolean }>(`${path}/${id}`, {}).toPromise();
  }

  public getConfig(id: string, type = NodeType.Cycle) {
    return this.http.get<IConfig>(`${path}/${id}/${type}`).toPromise();
  }

  public updateConfig(id: string, type = NodeType.Cycle, config: IOrchestratorConfig) {
    return this.http.put<{ success: boolean }>(`${path}/${id}/${type}`, config).toPromise();
  }

  public deleteConfig(id: string) {
    return this.http.delete<{ success: boolean }>(`${path}/${id}`).toPromise();
  }
}
