import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITermJSONLD, NodeType, JSON as HestiaJson } from '@hestia-earth/schema';
import { DataState, nodeTypeToParam } from '@hestia-earth/api';
import { filterParams } from '@hestia-earth/ui-components';
import { IValidationError } from '@hestia-earth/ui-components/files/files-error.model';

import { environment } from '../environments/environment';

export interface IBaseResult {
  id: string;
  createdOn: Date;
}

export interface IMetadata {
  id: string;
  createdOn?: number;
  recalculatedOn?: number;
  error?: {
    error: string;
    stack?: string;
  };
  config?: {
    /**
     * Configuration used to recalculate.
     */
    config?: string;
    /**
     * Version of the models used to recalculate.
     */
    version?: string;
  };
  /**
   * Path of the Source file, if any. Can be used with `/download` endpoint.
   */
  sourcePath?: string;
}

export class SearchResult {
  results: IMetadata[] = [];
  count = 0;
}

export type sortByFields = keyof IMetadata;
export type sortByOrder = 'asc' | 'desc';

export interface ISearchParams {
  search?: string;
  offset?: number;
  limit?: number;
  sortOrder?: sortByOrder;
  sortBy?: sortByFields;
}

export interface ICollection {
  name: string;
  collection: string;
  ee_type: 'raster' | 'raster_by_period' | 'vector';
  reducer?: 'mean' | 'mode' | 'first' | 'sum';
  fields: string;
  field?: string;
  band_name?: string;
}

export interface ICollectionExtended extends ICollection {
  max_area: number;
  gadm_id?: string;
  latitude?: number;
  longitude?: string;
  year: number;
  years?: number[];
}

export interface IDraft extends IBaseResult {
  nodes: HestiaJson<any>[];
  sourcePath?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient
  ) { }

  public calculate(data: any, config?: string) {
    return this.http.post<any>(`${environment.apiUrl}/recalculate`, data, {
      headers: filterParams({ config })
    }).toPromise();
  }

  public list(type = NodeType.ImpactAssessment, params: ISearchParams = {}) {
    return this.http.get<SearchResult>(`${environment.apiUrl}/${nodeTypeToParam(type)}`, {
      params: filterParams(params)
    }).toPromise();
  }

  public get<T>(type = NodeType.ImpactAssessment, id: string, dataState = DataState.original) {
    return this.http.get<T>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}`, {
      params: { dataState }
    }).toPromise().catch(() => ({} as T));
  }

  public getMetadata(type = NodeType.ImpactAssessment, id: string) {
    return this.http.get<IMetadata>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}/metadata`).toPromise();
  }

  public calculateNode(type = NodeType.ImpactAssessment, id: string, config?: string) {
    return this.http.post<any>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}/pipeline`, {}, {
      headers: filterParams({ config })
    }).toPromise();
  }

  public delete(type = NodeType.ImpactAssessment, id: string) {
    return this.http.delete<{ success: boolean }>(
      `${environment.apiUrl}/${nodeTypeToParam(type)}/${id}`
    ).toPromise();
  }

  public getTerm(id: string) {
    return this.http.get<ITermJSONLD>(`${environment.apiUrl}/terms/${id}`).toPromise();
  }

  public uploadFile(type: NodeType, id: string, file: File) {
    const formData = new FormData();
    formData.append('file', file, 'file.csv');
    formData.append('id', id);
    formData.append('type', type);
    return this.http.post<void>(`${environment.apiUrl}/upload`, formData).toPromise();
  }

  public convertExcel(file: File) {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<string>(`${environment.apiUrl}/upload/excel`, formData).toPromise();
  }

  public ee(type: 'gadm' | 'coordinates' | 'boundary', { years, ...data }: ICollectionExtended) {
    return this.http.post<any>(`${environment.apiUrl}/ee/${type}`, data).toPromise();
  }

  public validate(nodes: HestiaJson<any>[]) {
    return this.http.post<IValidationError[][]>(`${environment.apiUrl}/validate`, { nodes }).toPromise();
  }

  // Drafts

  public listDrafts(params: ISearchParams = {}) {
    return this.http.get<SearchResult>(`${environment.apiUrl}/drafts`, {
      params: filterParams(params)
    }).toPromise();
  }

  public getDraft(id: string) {
    return this.http.get<IDraft>(`${environment.apiUrl}/drafts/${id}`).toPromise();
  }

  /**
   * Upload draft file.
   *
   * @param id The id of the draft (using the source filename)
   * @param file The content of the file
   */
  public createDraft(id: string, file: File) {
    const formData = new FormData();
    formData.append('file', file, 'file.csv');
    formData.append('id', id);
    return this.http.post<void>(`${environment.apiUrl}/drafts`, formData).toPromise();
  }

  public updateDraft(id: string, nodes: HestiaJson<any>[]) {
    return this.http.put<{ success: boolean }>(`${environment.apiUrl}/drafts/${id}`, { nodes }).toPromise();
  }

  public deleteDraft(id: string) {
    return this.http.delete<{ success: boolean }>(`${environment.apiUrl}/drafts/${id}`).toPromise();
  }

  /**
   * When converting a draft into a calculations.
   * Copy the Draft source file over to the result folder.
   *
   * @param draft
   * @param type
   * @param id
   * @returns
   */
  public submitDraft(draft: Partial<IDraft>, type = NodeType.ImpactAssessment, id: string) {
    return this.http.post<void>(`${environment.apiUrl}/drafts/${draft.id}`, {
      type, id
    }).toPromise().catch(() => {});
  }
}
