import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ApiService } from '../../api.service';

@Component({
  selector: 'app-results-draft-delete-confirm',
  templateUrl: './results-draft-delete-confirm.component.html',
  styleUrls: ['./results-draft-delete-confirm.component.scss']
})
export class ResultsDraftDeleteConfirmComponent {
  @Input()
  public id = '';
  @Output()
  public closed = new EventEmitter<boolean>();

  public deleting = false;

  constructor(
    private service: ApiService
  ) { }

  public async confirm() {
    this.deleting = true;
    await this.service.deleteDraft(this.id);
    this.closed.next(true);
  }
}
