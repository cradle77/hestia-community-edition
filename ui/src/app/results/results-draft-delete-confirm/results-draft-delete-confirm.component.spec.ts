import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsDraftDeleteConfirmComponent } from './results-draft-delete-confirm.component';

describe('ResultsDraftDeleteConfirmComponent', () => {
  let component: ResultsDraftDeleteConfirmComponent;
  let fixture: ComponentFixture<ResultsDraftDeleteConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsDraftDeleteConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsDraftDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
