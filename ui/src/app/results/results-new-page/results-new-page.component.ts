import { Component, OnInit } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { Cycle, NodeType, SchemaType } from '@hestia-earth/schema';
import { definitions } from '@hestia-earth/json-schema';
import { HeSchemaService, schemaTypeToDefaultValue, Repository } from '@hestia-earth/ui-components';

import { IDraft } from '../../api.service';

const defaultCycleData = (id = `cycle-${new Date().getTime()}`) => ({
  type: NodeType.Cycle,
  id,
  dataPrivate: true,
  endDate: new Date().getFullYear(),
  site: {
    type: NodeType.Site,
    id: `site-${id}`,
    country: {
      type: NodeType.Term,
      name: ''
    },
    region: {
      type: NodeType.Term,
      name: ''
    }
  },
  products: [
    {
      '@type': SchemaType.Product,
      primary: true,
      economicValueShare: 100,
      value: '',
      term: {
        type: NodeType.Term,
        name: ''
      }
    }
  ],
  inputs: [
    {
      '@type': SchemaType.Input,
      value: '',
      term: {
        type: NodeType.Term,
        name: ''
      }
    }
  ]
});

@Component({
  selector: 'app-results-new-page',
  templateUrl: './results-new-page.component.html',
  styleUrls: ['./results-new-page.component.scss']
})
export class ResultsNewPageComponent implements OnInit {
  private schemas?: definitions;

  public Repository = Repository;
  public loading = true;
  public cycle?: Cycle;
  public draft: Partial<IDraft> = {
    id: `draft-${uuidv4()}`
  };
  public showReportIssue = false;

  constructor(
    private schemaService: HeSchemaService
  ) { }

  async ngOnInit() {
    this.schemas = await this.schemaService.schemas();
    this.addCycle();
    this.loading = false;
  }

  private addCycle() {
    const { defaultSource, ...data } = schemaTypeToDefaultValue(this.schemas!, this.schemas![NodeType.Cycle]);
    this.cycle = {
      ...data,
      ...defaultCycleData()
    };
  }
}
