import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsNewPageComponent } from './results-new-page.component';

describe('ResultsNewPageComponent', () => {
  let component: ResultsNewPageComponent;
  let fixture: ComponentFixture<ResultsNewPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsNewPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
