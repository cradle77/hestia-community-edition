import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NodeType } from '@hestia-earth/schema';
import { scrollToEl, filterParams } from '@hestia-earth/ui-components';

import { ApiService, SearchResult, IMetadata, sortByFields, sortByOrder } from '../../api.service';

enum Tabs {
  results = 'ImpactAssessment',
  drafts = 'Draft'
}

@Component({
  selector: 'app-results-list-page',
  templateUrl: './results-list-page.component.html',
  styleUrls: ['./results-list-page.component.scss']
})
export class ResultsListPageComponent implements OnInit {
  public loading = true;
  public NodeType = NodeType;
  public selectedType = NodeType.ImpactAssessment;
  public Tabs = Tabs;
  public selectedTab = Tabs.results;

  public search = '';
  public perPage = 10;
  public page = 1;
  public showSortBy = false;
  public sortBy: sortByFields = 'createdOn';
  public sortOrder: sortByOrder = 'desc';
  public results = new SearchResult();

  public showActions?: IMetadata;
  public showRecalculate?: IMetadata;
  public showDelete?: IMetadata;
  public showError?: IMetadata;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public service: ApiService
  ) { }

  ngOnInit() {
    const params = this.route.snapshot.queryParamMap;
    this.search = params.get('query') || '';
    this.sortBy = params.get('sortBy') as sortByFields || 'createdOn';
    this.sortOrder = params.get('sortOrder') as sortByOrder || 'desc';
    this.page = +(params.get('page') || '1');
    return this.filter();
  }

  public scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  public trackByResult(_index: number, { id }: IMetadata) {
    return id;
  }

  sort(sortBy: sortByFields) {
    this.sortOrder = this.sortBy === sortBy ? (
      this.sortOrder === 'asc' ? 'desc' : 'asc'
    ) : 'desc';
    this.sortBy = sortBy;
    return this.runSearch();
  }

  private async updateQueryParams() {
    const queryParams = this.queryParams;
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      ...(Object.keys(queryParams).length ? { queryParams } : {})
    });
  }

  public async runSearch() {
    this.page = 1;
    return this.filter();
  }

  public async filter() {
    await this.updateQueryParams();
    this.loading = true;
    this.results = this.selectedTab === Tabs.drafts ?
      await this.service.listDrafts(this.searchParams) :
      await this.service.list(this.selectedType, this.searchParams);
    this.loading = false;
  }

  public get queryParams() {
    return filterParams({
      query: this.search,
      page: this.page,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    });
  }

  private get searchParams() {
    return {
      search: this.search,
      limit: this.perPage,
      offset: this.offset,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    };
  }

  public get total() {
    return this.results?.count ?? 0;
  }

  public get offset() {
    return (this.page - 1) * this.perPage;
  }

  public get isDraft() {
    return this.selectedTab === Tabs.drafts;
  }

  public toggleShowActions(result: IMetadata) {
    this.showActions = this.showActions === result ? undefined : result;
  }

  public deleteConfirmed(confirmed: boolean) {
    this.showDelete = undefined;
    return confirmed ? this.router.navigate(['/results']) : null;
  }

  public recalculateConfirmed(confirmed: boolean) {
    this.showRecalculate = undefined;
    return confirmed ? this.runSearch() : null;
  }
}
