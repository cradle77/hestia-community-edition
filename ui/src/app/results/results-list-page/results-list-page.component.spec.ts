import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsListPageComponent } from './results-list-page.component';
import { ApiService } from '../../api.service';

class FakeService {}

describe('ResultsListPageComponent', () => {
  let component: ResultsListPageComponent;
  let fixture: ComponentFixture<ResultsListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsListPageComponent ],
      providers: [
        { provide: ApiService, useValue: FakeService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
