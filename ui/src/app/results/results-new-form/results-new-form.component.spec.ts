import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsNewFormComponent } from './results-new-form.component';

describe('ResultsNewFormComponent', () => {
  let component: ResultsNewFormComponent;
  let fixture: ComponentFixture<ResultsNewFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsNewFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsNewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
