import { Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { debounceTime, delay, distinctUntilChanged, map, mergeMap, switchMap, tap, toArray } from 'rxjs/operators';
import set from 'lodash.set';
import { JSON as HestiaJson } from '@hestia-earth/schema';
import { definitions } from '@hestia-earth/json-schema';
import {
  HeSchemaService, errorText, scrollTop, FilesFormComponent, buildSummary, baseUrl
} from '@hestia-earth/ui-components';
import { IValidationError } from '@hestia-earth/ui-components/files/files-error.model';
import { ISummary } from '@hestia-earth/ui-components/files/files-error-summary.model';
import { validator } from '@hestia-earth/schema-validation/validate';

import { ApiService, IDraft } from '../../api.service';
import { ApiConfigService, defaultConfigId } from '../../api-config.service';

const schemaValidation = validator(baseUrl(), false);

const openNodeChildren = (node: FilesFormComponent) => {
  const groups = node.ref.nativeElement.querySelectorAll('.open-group:not(.is-open)');
  groups.forEach((group: any) => group.click());
};

@Component({
  selector: 'app-results-new-form',
  templateUrl: './results-new-form.component.html',
  styleUrls: ['./results-new-form.component.scss']
})
export class ResultsNewFormComponent implements OnInit, OnChanges {
  private validationErrors?: IValidationError[][];

  @ViewChildren(FilesFormComponent)
  private forms?: QueryList<FilesFormComponent>;

  @Input()
  public draft?: Partial<IDraft>;
  @Input()
  public nodes: HestiaJson<any>[] = [];

  public scrollTop = scrollTop;
  public schemas?: definitions;
  public page = 1;
  public perPage = 10;

  public loading = true;
  public submitted = false;
  public processing = false;
  public validating = false;
  public error?: string;

  public showSummary = true;
  public summaryData: ISummary[] = [];

  public showSelectConfig = false;
  public config = defaultConfigId;

  public suggestConfig = (text$: Observable<string>) => text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    switchMap(search => from(this.configService.listConfigs({ search })).pipe(
      map(results => results.results.map(({ id }) => id))
    ))
  );

  constructor(
    private router: Router,
    private schemaService: HeSchemaService,
    private service: ApiService,
    private configService: ApiConfigService
  ) { }

  async ngOnInit() {
    this.schemas = await this.schemaService.schemas();
    this.showSelectConfig = (await this.configService.listConfigs({ limit: 1 })).count > 1;
    this.loading = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('nodes' in changes) {
      setTimeout(() => this.openNodes(), 100);
    }
  }

  public trackByIndex(index: number, _node: HestiaJson<any>) {
    return index;
  }

  public get progressOffset() {
    return (this.page - 1) * this.perPage;
  }

  public get progressLimit() {
    return this.progressOffset + this.perPage;
  }

  private openNodes() {
    const [form] = (this.forms || []);
    return form && openNodeChildren(form);
  }

  public nodeChanged({ key, value }: any, index: number) {
    if (key) {
      set(this.nodes[index], key, value);
    }
  }

  public removeNode(index: number) {
    this.nodes.splice(index, 1);
  }

  public get nodeMap() {
    return this.nodes.reduce((prev, curr) => {
      prev[curr.type] = prev[curr.type] || [];
      prev[curr.type].push(curr.id);
      return prev;
    }, {} as any);
  }

  private async uploadNode(node: HestiaJson<any>) {
    await this.service.calculate(node, this.config);
    return this.draft ?
      await this.service.submitDraft(this.draft, node.type, node.id!) :
      null;
  }

  public async submit() {
    this.error = undefined;
    this.submitted = true;
    await this.validateSchema();
    if (this.hasValidationError) {
      return;
    }

    this.processing = true;
    try {
      await Promise.all(this.nodes.map(node => this.uploadNode(node)));
      const singleNode = this.nodes.length === 1 ? this.nodes[0] : null;
      return this.router.navigate([
        '/', 'results',
        ...(singleNode ? [singleNode.type, singleNode.id] : [])
      ]);
    }
    catch (err: any) {
      this.error = errorText(err);
    }
    finally {
      this.processing = false;
    }
  }

  public async saveDraft() {
    this.error = undefined;
    if (!this.draft?.id) {
      return;
    }

    this.processing = true;
    try {
      await this.service.updateDraft(this.draft.id, this.nodes);
      // make sure we are editing using the id
      this.router.navigate(['/results', this.draft.id, 'edit']);
    }
    catch (err: any) {
      this.error = errorText(err);
    }
    finally {
      this.processing = false;
    }
  }

  // Validation

  public get hasValidationError() {
    return (this.validationErrors || []).some(errors => errors.some(error => error.level === 'error'));
  }

  public async validateSchema() {
    this.validationErrors = await from(this.nodes).pipe(
      tap(() => this.validating = true),
      delay(300), // make sure we reload the forms and show a progress
      mergeMap(node => from(schemaValidation(node))),
      map(({ errors }) => errors.map(error => ({
        ...error,
        level: 'error'
      } as IValidationError))),
      toArray(),
      tap(() => this.validating = false)
    ).toPromise();

    this.summaryData = await buildSummary(this.nodes, this.validationErrors).toPromise();
  }

  public nodeErrors(index: number) {
    return (this.validationErrors || [])[this.progressOffset + index] || [];
  }

  public get noValidationErrors() {
    return this.validationErrors &&
      this.validationErrors.length > 0 &&
      this.validationErrors.every(errors => errors.length === 0);
  }

  public async validate() {
    // cannot validate when processing
    if (this.processing) {
      return;
    }
    // make sure schema is valid first
    await this.validateSchema();
    if (this.hasValidationError) {
      return;
    }

    this.error = undefined;
    this.validating = true;
    try {
      this.validationErrors = await this.service.validate(this.nodes);
      this.summaryData = await buildSummary(this.nodes, this.validationErrors).toPromise();
    }
    catch (err: any) {
      this.error = errorText(err);
    }
    finally {
      this.validating = false;
    }
  }
}
