import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ResultsDetailsPageComponent } from './results-details-page/results-details-page.component';
import { ResultsEditPageComponent } from './results-edit-page/results-edit-page.component';
import { ResultsListPageComponent } from './results-list-page/results-list-page.component';
import { ResultsNewPageComponent } from './results-new-page/results-new-page.component';
import { ResultsUploadPageComponent } from './results-upload-page/results-upload-page.component';

const routes: Routes = [
  {
    path: '',
    component: ResultsListPageComponent
  },
  {
    path: 'new',
    component: ResultsNewPageComponent
  },
  {
    path: 'upload',
    component: ResultsUploadPageComponent
  },
  {
    path: ':id/edit',
    component: ResultsEditPageComponent
  },
  {
    path: ':type/:id',
    component: ResultsDetailsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultsRoutingModule { }
