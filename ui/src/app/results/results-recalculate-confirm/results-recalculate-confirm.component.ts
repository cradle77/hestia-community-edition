import { Component, EventEmitter, Input, Output } from '@angular/core';
import { from, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { errorText } from '@hestia-earth/ui-components';
import { NodeType } from '@hestia-earth/schema';

import { ApiService } from '../../api.service';
import { ApiConfigService, defaultConfigId } from '../../api-config.service';

@Component({
  selector: 'app-results-recalculate-confirm',
  templateUrl: './results-recalculate-confirm.component.html',
  styleUrls: ['./results-recalculate-confirm.component.scss']
})
export class ResultsRecalculateConfirmComponent {
  @Input()
  public type = NodeType.ImpactAssessment;
  @Input()
  public id = '';
  @Input()
  public config = defaultConfigId;

  @Output()
  public closed = new EventEmitter<boolean>();

  public loading = false;
  public error?: string;

  public suggestConfig = (text$: Observable<string>) => text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    switchMap(search => from(this.configService.listConfigs({ search })).pipe(
      map(results => results.results.map(({ id }) => id))
    ))
  );

  constructor(
    private service: ApiService,
    private configService: ApiConfigService
  ) { }

  public async confirm() {
    this.loading = true;
    try {
      await this.service.calculateNode(this.type, this.id, this.config);
      this.closed.next(true);
    }
    catch (err: any) {
      this.error = err?.error?.detail;
    }
  }
}
