import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsRecalculateConfirmComponent } from './results-recalculate-confirm.component';

describe('ResultsRecalculateConfirmComponent', () => {
  let component: ResultsRecalculateConfirmComponent;
  let fixture: ComponentFixture<ResultsRecalculateConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsRecalculateConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsRecalculateConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
