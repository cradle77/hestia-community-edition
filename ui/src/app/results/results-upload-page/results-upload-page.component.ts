import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from, Subscription } from 'rxjs';
import { distinct, filter, map, mergeMap, reduce } from 'rxjs/operators';
import { replaceInvalidChars, SupportedExtensions, filenameWithoutExt } from '@hestia-earth/api';
import {
  NodeType, SchemaType, JSON as HestiaJson, isExpandable,
  ITermJSONLD, ISiteJSONLD, ImpactAssessmentAllocationMethod, ImpactAssessment, Cycle
} from '@hestia-earth/schema';
import { convertZip as convertOlca } from '@hestia-earth/olca';
import { convert as convertPooreNemecek } from '@hestia-earth/poore-nemecek';
import {
  HeNodeCsvService, isChrome, gitHome, Repository, baseUrl,
  HeSearchService, matchType, matchNameNormalized
} from '@hestia-earth/ui-components';

import { ApiService } from '../../api.service';
import { ApiVersionService } from '../../api-version.service';
import { ApiSettingsService, DataFormat } from '../../api-settings.service';

const now = () => new Date().getTime();

const formatId = (id: string) => id.replace(/[\s,\(\)\/\\]/g, '').toLowerCase();

const formatNode = ({ defaultSource, source, id: originalId, ...node }: any, id: string) => ({
  ...node,
  id,
  ...('site' in node ? {
    site: {
      type: SchemaType.Site,
      id
    }
  } : {}),
  originalId,
  dataPrivate: true
});

const updateLinkedImpactAssessment = (idMap: { [originalId: string]: string }) => (impact: ImpactAssessment) => ({
  ...impact,
  cycle: {
    ...impact.cycle,
    ...(impact.cycle?.inputs ? {
      inputs: impact.cycle.inputs.map(input => ({
        ...input,
        ...(input.impactAssessment ? {
          impactAssessment: {
            type: SchemaType.ImpactAssessment,
            id: idMap[input.impactAssessment?.id!] || input.impactAssessment.id
          }
        } : {})
      }))
    } : {})
  }
});

const nodeId = (product?: any, endDate?: string, site?: ISiteJSONLD) => formatId([
  product?.['@id'],
  site?.region?.name,
  site?.country?.name,
  endDate,
  now()
].filter(Boolean).join('-'));

const filterNodes = (nodes: any[], type: NodeType) => nodes.filter(node => node.type === type);

const findNode = (nodes: any[], type: NodeType, id: string) =>
  nodes.find(node => node.type === type && node.id === id);

const mergeNodesByImpactAssessment = ({ nodes }: any) => {
  const impacts: ImpactAssessment[] = filterNodes(nodes, NodeType.ImpactAssessment);
  const cycles: Cycle[] = filterNodes(nodes, NodeType.Cycle);
  // find all uploaded ImpactAssessments
  const uploadedImpacts = (impacts || []).map(impact => {
    const site = impact.site?.id ? findNode(nodes, NodeType.Site, impact.site.id) : null;
    const cycle = impact.cycle?.id ? findNode(nodes, NodeType.Cycle, impact.cycle.id) : null;
    const id = nodeId(impact?.product, impact.endDate, site);
    return {
      ...formatNode(impact, id),
      ...(site ? { site: formatNode(site, id) } : {}),
      ...(cycle ? { cycle: formatNode(cycle, id) } : {}),
      originalId: impact.id
    } as ImpactAssessment;
  });
  const uploadedImpactMap = uploadedImpacts.reduce((prev, curr) => ({ ...prev, [curr.originalId!]: curr.id }), {});
  return [
    ...uploadedImpacts,
    // wrap all Cycles that do not have an ImpactAssessment
    ...(cycles || []).map(cycle => {
      const site = cycle.site?.id ? findNode(nodes, NodeType.Site, cycle.site.id) : null;
      const product = cycle.products?.[0];
      const id = nodeId(cycle.products?.[0]?.term, cycle.endDate, site);
      const hasImpact = impacts.some(impact => impact?.cycle?.id === cycle.id);
      return hasImpact
        ? (undefined as unknown) as ImpactAssessment
        : {
          type: SchemaType.ImpactAssessment,
          id,
          cycle: formatNode(cycle, id),
          site: site ? formatNode(site, id) : undefined,
          product: product?.term,
          endDate: cycle.endDate,
          country: site?.country,
          functionalUnitQuantity: 1,
          allocationMethod: ImpactAssessmentAllocationMethod.none,
          dataPrivate: true
        } as ImpactAssessment;
    })
  ].filter(Boolean).map(updateLinkedImpactAssessment(uploadedImpactMap));
};

const supportedExtensions: {
  [format in DataFormat]: string[];
} = {
  [DataFormat.Hestia]: [`.${SupportedExtensions.csv}`],
  [DataFormat.OpenLCA]: ['.zip'],
  [DataFormat.PooreNemecek]: [`.${SupportedExtensions.csv}`]
};

const isExcel = ({ name }: File) =>
  name?.endsWith(SupportedExtensions.xls) ||
  name?.endsWith(SupportedExtensions.xlsx);

const isTerm = (value: any) => (value.type || value['@type']) === NodeType.Term;

const findDeepTermNames = (node: any): string[] =>
  Object.values(node).filter(isExpandable).flatMap((value: any) => {
    const isArray = Array.isArray(value);
    return isArray
      ? value.flatMap(val => isExpandable(val) ? findDeepTermNames(val) : val)
      : isTerm(value)
        ? value.name
        : null;
  });

const deepReplaceTerms = (node: any, termsByName: { [name: string]: ITermJSONLD }) =>
  Object.fromEntries(
    Object.entries(node)
      .map(([key, value]: [string, any]) => {
        const expandable = isExpandable(value);
        const isArray = Array.isArray(value);
        const res: any = expandable
          ? isArray
            ? value.map(val => isExpandable(val) ? deepReplaceTerms(val, termsByName) : val)
            : isTerm(value)
              ? termsByName[value?.name] || value
              : value
          : value;
        return [key, res];
      })
  );

const parseConvertError = (error: Error) => {
  try {
    return JSON.parse(error.message);
  }
  catch (err) {
    return error.stack || error.message;
  }
};

@Component({
  selector: 'app-results-upload-page',
  templateUrl: './results-upload-page.component.html',
  styleUrls: ['./results-upload-page.component.scss']
})
export class ResultsUploadPageComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private enableExcelUpload = false;

  public isChrome = isChrome();
  public baseUrl = baseUrl();
  public gitHome = gitHome;
  public Repository = Repository;

  public id?: string;
  public file?: File;
  public filename?: string;
  public DataFormat = DataFormat;
  public dataFormat = DataFormat.PooreNemecek;
  public submitted = false;
  public loading = false;
  public nodes: HestiaJson<any>[] = [];
  public error?: string;
  public showReportIssue = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private nodeCsvService: HeNodeCsvService,
    private searchService: HeSearchService,
    private service: ApiService,
    private settingsService: ApiSettingsService,
    versionService: ApiVersionService
  ) {
    this.subscriptions.push(versionService.pandasVersion$.subscribe(() => this.enableExcelUpload = true));
  }

  async ngOnInit() {
    const params = this.route.snapshot.queryParamMap;
    this.id = params.get('id') || undefined;
    this.dataFormat = await this.settingsService.getSingle('defaultFormat');
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public addFiles({ files }: any) {
    if (files && files.length) {
      const [file]: File[] = Array.from(files);
      this.file = file;
      this.filename = file.name;
    }
  }

  public get supportedExtensions() {
    const exts = supportedExtensions[this.dataFormat].slice();
    if (this.enableExcelUpload && exts.includes(`.${SupportedExtensions.csv}`)) {
      exts.push(`.${SupportedExtensions.xls}`);
      exts.push(`.${SupportedExtensions.xlsx}`);
    }
    return exts;
  }

  private async findTerm(name: string) {
    const { results } = await this.searchService.search<ITermJSONLD, NodeType.Term>({
      limit: 1,
      fields: ['@type', '@id', 'name', 'termType'],
      query: {
        bool: {
          must: [
            matchType(NodeType.Term),
            matchNameNormalized(name)
          ]
        }
      }
    });
    return results?.[0];
  }

  private async findTerms(nodes: any[]) {
    return from(nodes).pipe(
      mergeMap(findDeepTermNames),
      distinct(),
      mergeMap(name => from(this.findTerm(name)).pipe(
        filter(term => !!term),
        map(({ _score, ...term}) => ({ name, term }))
      ), 10),
      reduce((prev, curr) => {
        prev[curr.name] = curr.term;
        return prev;
      }, {} as { [name: string]: ITermJSONLD })
    ).toPromise();
  }

  private async convertHestia(content: string) {
    const nodes = await this.nodeCsvService.csvToJson(content);
    const termsByName = await this.findTerms(nodes);
    return { nodes: nodes.map(node => deepReplaceTerms(node, termsByName)) };
  }

  private async convertToFormat(file: File) {
    const converter: {
      [format in DataFormat]: (fileContent: string) => Promise<{ nodes: any[] }>;
    } = {
      [DataFormat.Hestia]: fileContent => this.convertHestia(fileContent),
      [DataFormat.OpenLCA]: () => convertOlca(file).toPromise(),
      [DataFormat.PooreNemecek]: fileContent => convertPooreNemecek(fileContent, true)
    };
    const content = isExcel(file) ? await this.service.convertExcel(file) : await file.text();
    return mergeNodesByImpactAssessment(await converter[this.dataFormat](content));
  }

  public async submit() {
    this.submitted = true;
    if (!this.file) {
      return;
    }

    this.loading = true;
    this.error = undefined;
    try {
      const id = this.id || replaceInvalidChars(filenameWithoutExt(this.filename));
      const nodes = await this.convertToFormat(this.file);
      await this.service.createDraft(id, this.file);
      await this.service.updateDraft(id, nodes);
      return this.router.navigate(['/', 'results', id, 'edit']);
    }
    catch (err: any) {
      console.error(err);
      this.error = parseConvertError(err);
    }
    finally {
      this.loading = false;
    }
  }
}
