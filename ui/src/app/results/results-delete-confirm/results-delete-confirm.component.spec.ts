import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsDeleteConfirmComponent } from './results-delete-confirm.component';

describe('ResultsDeleteConfirmComponent', () => {
  let component: ResultsDeleteConfirmComponent;
  let fixture: ComponentFixture<ResultsDeleteConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsDeleteConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
