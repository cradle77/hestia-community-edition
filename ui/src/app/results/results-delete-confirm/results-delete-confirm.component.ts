import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { errorText } from '@hestia-earth/ui-components';

import { ApiService } from '../../api.service';

@Component({
  selector: 'app-results-delete-confirm',
  templateUrl: './results-delete-confirm.component.html',
  styleUrls: ['./results-delete-confirm.component.scss']
})
export class ResultsDeleteConfirmComponent {
  @Input()
  public type = NodeType.ImpactAssessment;
  @Input()
  public id = '';
  @Output()
  public closed = new EventEmitter<boolean>();

  public loading = false;
  public error?: string;

  constructor(
    private service: ApiService
  ) { }

  public async confirm() {
    this.loading = true;
    try {
      await this.service.delete(this.type, this.id);
      this.closed.next(true);
    }
    catch (err) {
      this.error = errorText(err);
    }
  }
}
