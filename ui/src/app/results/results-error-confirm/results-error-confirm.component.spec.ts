import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsErrorConfirmComponent } from './results-error-confirm.component';

describe('ResultsErrorConfirmComponent', () => {
  let component: ResultsErrorConfirmComponent;
  let fixture: ComponentFixture<ResultsErrorConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsErrorConfirmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultsErrorConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
