import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-results-error-confirm',
  templateUrl: './results-error-confirm.component.html',
  styleUrls: ['./results-error-confirm.component.scss']
})
export class ResultsErrorConfirmComponent {
  @Input()
  public error?: string;

  @Output()
  public closed = new EventEmitter<boolean>();
}
