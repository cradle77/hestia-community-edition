import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  HeNodeModule, HeFilesModule, HeCyclesModule, HeSitesModule, HeImpactAssessmentsModule
} from '@hestia-earth/ui-components';

import { SharedModule } from '../shared/shared.module';
import { ResultsRoutingModule } from './results-routing.module';

import { ResultsDetailsPageComponent } from './results-details-page/results-details-page.component';
import { ResultsListPageComponent } from './results-list-page/results-list-page.component';
import { ResultsDeleteConfirmComponent } from './results-delete-confirm/results-delete-confirm.component';
import { ResultsNewPageComponent } from './results-new-page/results-new-page.component';
import { ResultsUploadPageComponent } from './results-upload-page/results-upload-page.component';
import { ResultsNewFormComponent } from './results-new-form/results-new-form.component';
import { ResultsEditPageComponent } from './results-edit-page/results-edit-page.component';
import { ResultsDraftDeleteConfirmComponent } from './results-draft-delete-confirm/results-draft-delete-confirm.component';
import { ResultsRecalculateConfirmComponent } from './results-recalculate-confirm/results-recalculate-confirm.component';
import { ResultsModelsVersionComponent } from './results-models-version/results-models-version.component';
import { ResultsErrorComponent } from './results-error/results-error.component';
import { ResultsErrorConfirmComponent } from './results-error-confirm/results-error-confirm.component';

@NgModule({
  declarations: [
    ResultsDetailsPageComponent,
    ResultsListPageComponent,
    ResultsDeleteConfirmComponent,
    ResultsNewPageComponent,
    ResultsUploadPageComponent,
    ResultsNewFormComponent,
    ResultsEditPageComponent,
    ResultsDraftDeleteConfirmComponent,
    ResultsRecalculateConfirmComponent,
    ResultsModelsVersionComponent,
    ResultsErrorComponent,
    ResultsErrorConfirmComponent
  ],
  imports: [
    CommonModule, FormsModule,
    SharedModule,
    ResultsRoutingModule,
    HeNodeModule,
    HeCyclesModule,
    HeImpactAssessmentsModule,
    HeSitesModule,
    HeFilesModule
  ]
})
export class ResultsModule { }
