import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { ApiService, IDraft } from '../../api.service';

@Component({
  selector: 'app-results-edit-page',
  templateUrl: './results-edit-page.component.html',
  styleUrls: ['./results-edit-page.component.scss']
})
export class ResultsEditPageComponent implements OnInit {
  public loading = true;
  public id?: string;
  public draft?: IDraft;
  public showActions = false;
  public showDelete = false;
  public sourceUrl?: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: ApiService
  ) { }

  async ngOnInit() {
    const { id } = await this.route.params.pipe(take(1)).toPromise();
    this.id = id;
    this.draft = await this.service.getDraft(id);
    this.updateSource();
    this.loading = false;
  }

  public deleteConfirmed(confirmed: boolean) {
    this.showDelete = false;
    return confirmed ? this.router.navigate(['/results']) : null;
  }

  private async updateSource() {
    const path = this.draft?.sourcePath ? encodeURIComponent(this.draft?.sourcePath) : null;
    this.sourceUrl = path ? `${environment.apiUrl}/download?path=${path}` : undefined;
  }
}
