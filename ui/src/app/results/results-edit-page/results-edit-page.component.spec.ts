import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsEditPageComponent } from './results-edit-page.component';

describe('ResultsEditPageComponent', () => {
  let component: ResultsEditPageComponent;
  let fixture: ComponentFixture<ResultsEditPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsEditPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
