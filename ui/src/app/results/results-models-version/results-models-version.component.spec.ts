import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsModelsVersionComponent } from './results-models-version.component';

describe('ResultsModelsVersionComponent', () => {
  let component: ResultsModelsVersionComponent;
  let fixture: ComponentFixture<ResultsModelsVersionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsModelsVersionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultsModelsVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
