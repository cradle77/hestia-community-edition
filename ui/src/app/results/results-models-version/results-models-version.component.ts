import { Component, Input, OnInit } from '@angular/core';
import { engineGitBaseUrl } from '@hestia-earth/ui-components';

import { ApiVersionService } from '../../api-version.service';

@Component({
  selector: 'app-results-models-version',
  templateUrl: './results-models-version.component.html',
  styleUrls: ['./results-models-version.component.scss']
})
export class ResultsModelsVersionComponent implements OnInit {
  @Input()
  public version?: string;

  public latestVersion?: string;

  constructor(
    private versionService: ApiVersionService
  ) { }

  async ngOnInit() {
    this.latestVersion = (await this.versionService.getVersions())?.models;
  }

  public get url() {
    return `${engineGitBaseUrl()}/-/tree/v${this.version}?ref_type=tags`;
  }
}
