import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ICycleJSONLD, IImpactAssessmentJSONLD, ISiteJSONLD, NodeType, TermTermType } from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import { scrollToEl, nodeVersion } from '@hestia-earth/ui-components';
import { propertyValue } from '@hestia-earth/utils/dist/term';

import { environment } from '../../../environments/environment';
import { ApiService, IMetadata } from '../../api.service';
import { defaultConfigId } from '../../api-config.service';

enum Tabs {
  cycle = 'Cycle',
  site = 'Site',
  impact = 'Impact Assessment'
}

const sharedSections: { [section: string]: string } = {
  logs: 'All Logs',
  lookups: 'Missing Lookups',
  jsonld: 'JSON-LD'
};

const sections: {
  [tab in Tabs]: { [section: string]: string }
} = {
  [Tabs.cycle]: {
    cycleCompleteness: 'Data Completeness',
    cycleActivity: 'Inputs & Products',
    cyclePractices: 'Practices',
    cycleEmissions: 'Emissions',
    ...sharedSections
  },
  [Tabs.site]: {
    siteMeasurements: 'Measurements',
    ...sharedSections
  },
  [Tabs.impact]: {
    impactEmissions: 'Emissions',
    impactResourceUse: 'Resource Use',
    impactImpacts: 'Impact Indicators',
    impactDriverChart: 'Driver Chart',
    ...sharedSections
  }
};

interface IDataState {
  cycle?: ICycleJSONLD;
  impact?: IImpactAssessmentJSONLD;
  site?: ISiteJSONLD;
}

type data = {
  [state in DataState]?: IDataState;
};

const getNodeValue = (node: any, arrayKey: string, term: string, factor = 1, method?: string) => {
  const blankNode = (node[arrayKey] || []).find((v: any) => v.term['@id'] === term && (
    !method || !v.methodModel || v.methodModel['@id'] === method
  ));
  const value = propertyValue(blankNode?.value ?? [], blankNode?.term['@id']);
  return value === null ? '' : value * factor;
};

const convertToCsv = (impact: IImpactAssessmentJSONLD, date?: number) => [
  [
    'ID',
    'Date',
    'Land Use (m2*yr)',
    'Biodiv. (PDF.yr*10^14)',
    'GHG Emis Farm (kg CO2eq)',
    'Eutr Pot Farm (kg PO4 eq)',
    'Freshwtr Withdr Farm (L)',
    'Wtr Sc Farm (L eq)'
  ].join(','),
  [
    impact['@id'],
    date ? new Date(date).toJSON() : '',
    getNodeValue(impact, 'emissionsResourceUse', 'landOccupationDuringCycle'),
    getNodeValue(impact, 'impacts', 'damageToTerrestrialEcosystemsTotalLandUseEffects', Math.pow(10, 14)) ||
    getNodeValue(impact, 'impacts', 'damageToTerrestrialEcosystemsLandOccupation', Math.pow(10, 14)),
    getNodeValue(impact, 'impacts', 'gwp100', 1),
    getNodeValue(impact, 'impacts', 'eutrophicationPotentialExcludingFate'),
    getNodeValue(impact, 'emissionsResourceUse', 'freshwaterWithdrawalsDuringCycle'),
    getNodeValue(impact, 'impacts', 'scarcityWeightedWaterUse')
  ]
].join('\n');

@Component({
  selector: 'app-results-details-page',
  templateUrl: './results-details-page.component.html',
  styleUrls: ['./results-details-page.component.scss']
})
export class ResultsDetailsPageComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private fragment?: string;

  public loading = true;
  public collapsed = false;
  public DataState = DataState;
  public NodeType = NodeType;
  public TermTermType = TermTermType;

  public type?: NodeType;
  public id = '';
  public metadata?: IMetadata;
  public data?: data;
  public cycle?: ICycleJSONLD;
  public impact?: IImpactAssessmentJSONLD;
  public site?: ISiteJSONLD;
  public currentVersion?: string;

  public sourceUrl?: string;
  public impactsCSV?: SafeResourceUrl;

  public selectedDataState = DataState.original;
  public Tabs = Tabs;
  public selectedTab = Tabs.cycle;
  public sections = sections;

  public showRecalculated = true;
  public showActions = false;

  public sectionClosed: { [key: string]: boolean } = {};
  public showRecalculate = false;
  public showDelete = false;
  public showError = false;
  public showReportIssue = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private domSanitizer: DomSanitizer,
    private service: ApiService
  ) {
    this.subscriptions.push(this.route.params.subscribe(data => this.id && data.id !== this.id ? this.refresh() : null));
    this.subscriptions.push(this.route.fragment.subscribe(fragment => this.fragment = fragment || undefined));
  }

  async ngOnInit() {
    const { type, id } = await this.route.params.pipe(take(1)).toPromise();
    this.type = type as NodeType;
    this.id = id;
    await this.refresh();
    this.goToFragment();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private goToFragment() {
    setTimeout(() => scrollToEl(this.fragment!));
  }

  public async refresh() {
    this.loading = true;
    this.data = {
      [DataState.original]: await this.getData(DataState.original),
      [DataState.recalculated]: await this.getData(DataState.recalculated)
    };
    this.metadata = await this.service.getMetadata(this.type, this.id);
    this.selectDataState(DataState.original);

    this.updateSource();
    this.updateCSV();

    const recalculated = Object.values(this.data?.[DataState.recalculated] || {}).filter(Boolean);
    if (recalculated.length > 0) {
      this.currentVersion = this.metadata?.config?.version || nodeVersion(recalculated[0]);
      setTimeout(() => this.selectDataState(DataState.recalculated), 500); // simulate change of dataState
    }
    this.loading = false;
  }

  private async getData(dataState: DataState) {
    return {
      cycle: await this.service.get<ICycleJSONLD>(NodeType.Cycle, this.id, dataState),
      impact: await this.service.get<IImpactAssessmentJSONLD>(NodeType.ImpactAssessment, this.id, dataState),
      site: await this.service.get<ISiteJSONLD>(NodeType.Site, this.id, dataState),
    } as IDataState;
  }

  private async updateSource() {
    const path = this.metadata?.sourcePath ? encodeURIComponent(this.metadata?.sourcePath) : null;
    this.sourceUrl = path ? `${environment.apiUrl}/download?path=${path}` : undefined;
  }

  private updateCSV() {
    try {
      const impact = this.data?.[DataState.recalculated]?.impact;
      const content = impact ? convertToCsv(impact, this.metadata?.recalculatedOn) : null;
      this.impactsCSV = content ? this.domSanitizer.bypassSecurityTrustResourceUrl(
        `data:text/html;charset=utf-8,${encodeURIComponent(content)}`
      ) : undefined;
    }
    catch (err) {
      console.error(err);
    }
  }

  public get isDefaultConfig() {
    const config = this.metadata?.config?.config;
    return !config || config === defaultConfigId;
  }

  public selectDataState(dataState: DataState) {
    this.selectedDataState = dataState;
    this.cycle = this.data?.[dataState]?.cycle || this.data?.original?.cycle;
    this.site = this.data?.[dataState]?.site || this.data?.original?.site;
    this.impact = this.data?.[dataState]?.impact || this.data?.original?.impact;
  }

  public toggleDataState(recalculated: boolean) {
    return this.selectDataState(recalculated ? DataState.recalculated : DataState.original);
  }

  public deleteConfirmed(confirmed: boolean) {
    this.showDelete = false;
    return confirmed ? this.router.navigate(['/results']) : null;
  }

  public recalculateConfirmed(confirmed: boolean) {
    this.showRecalculate = false;
    return confirmed ? this.refresh() : null;
  }

  public isFragment(fragment: string) {
    return this.fragment === fragment;
  }
}
