import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsDetailsPageComponent } from './results-details-page.component';

describe('ResultsDetailsPageComponent', () => {
  let component: ResultsDetailsPageComponent;
  let fixture: ComponentFixture<ResultsDetailsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultsDetailsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
