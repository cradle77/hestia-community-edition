import { Component, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { gitHome, Repository } from '@hestia-earth/ui-components';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { ApiVersionService } from '../../api-version.service';

const issueLink = (url: string, error: string, version = 'N/A') =>
  `${gitHome}/${Repository.community}/-/issues/new?issue[title]=${
    encodeURIComponent('Issue running calculations')
  }&issue[description]=${
    encodeURIComponent(`
# 🐞 Bug Report

## Error

\`\`\`
${error}
\`\`\`

## Details

* API Version: \`${version}\`
* UI Version: \`${environment.version}\`
* URL: \`${url}\`

/label ~bug
/label ~"priority::MEDIUM"
    `.trim())
  }`;

@Component({
  selector: 'app-results-error',
  templateUrl: './results-error.component.html',
  styleUrls: ['./results-error.component.scss']
})
export class ResultsErrorComponent implements OnDestroy {
  private subscriptions: Subscription[] = [];
  private version?: string;

  @Input()
  public error?: string;

  constructor(
    private router: Router,
    private versionService: ApiVersionService
  ) {
    this.versionService.version$.subscribe(version => this.version = version);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public get issueLink() {
    return this.error ? issueLink(this.router.url, this.error, this.version) : null;
  }
}
