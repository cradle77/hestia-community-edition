import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {
  faAngleDown,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAngleLeft,
  faAngleRight,
  faAnglesLeft,
  faAnglesRight,
  faBug,
  faCheck,
  faCloudUploadAlt,
  faDownload,
  faEdit,
  faEllipsisV,
  faExclamationTriangle,
  faExternalLinkAlt,
  faLink,
  faLongArrowAltDown,
  faLongArrowAltUp,
  faPlus,
  faPlusCircle,
  faSave,
  faSearch,
  faSort,
  faSpinner,
  faSync,
  faTimes,
  faTimesCircle,
  faUpload
} from '@fortawesome/free-solid-svg-icons';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule
  ]
})
export class FontawesomeModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faAngleDown,
      faAngleDoubleLeft,
      faAngleDoubleRight,
      faAngleLeft,
      faAngleRight,
      faAnglesLeft,
      faAnglesRight,
      faBug,
      faCheck,
      faCloudUploadAlt,
      faDownload,
      faEdit,
      faEllipsisV,
      faExclamationTriangle,
      faExternalLinkAlt,
      faLink,
      faLongArrowAltDown,
      faLongArrowAltUp,
      faPlus,
      faPlusCircle,
      faSave,
      faSearch,
      faSort,
      faSpinner,
      faSync,
      faTimes,
      faTimesCircle,
      faUpload
    );
  }
}
