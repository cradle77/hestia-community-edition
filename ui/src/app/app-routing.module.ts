import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { TermsGuard } from './terms.guard';
import { path as termsPath } from './terms.model';

import { GeospatialSearchPageComponent } from './geospatial-search-page/geospatial-search-page.component';
import { TermsPageComponent } from './terms-page/terms-page.component';

const routes: Routes = [
  {
    path: 'results',
    loadChildren: () => import('./results/results.module').then(m => m.ResultsModule),
    canActivate: [TermsGuard]
  },
  {
    path: 'geospatial',
    component: GeospatialSearchPageComponent,
    canActivate: [TermsGuard]
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
    canActivate: [TermsGuard]
  },
  {
    path: termsPath,
    component: TermsPageComponent,
    canActivate: [TermsGuard]
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'results'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    anchorScrolling: 'enabled',
    useHash: false,
    enableTracing: false,
    relativeLinkResolution: 'legacy'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
