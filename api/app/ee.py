from typing import Any, Optional
from enum import Enum
from pydantic import BaseModel
from fastapi import APIRouter
from hestia_earth.earth_engine import run

router = APIRouter(
    prefix='/ee',
    tags=['Earth Engine'],
    responses={404: {'description': 'Not found'}},
)


class EEType(str, Enum):
    raster = 'raster'
    raster_by_period = 'raster_by_period'
    vector = 'vector'


class Reducer(str, Enum):
    mean = 'mean'
    mode = 'mode'
    first = 'first'
    sum = 'sum'


class Collection(BaseModel):
    collection: str
    ee_type: EEType
    fields: Optional[str] = None
    reducer: Optional[Reducer] = None
    field: Optional[str] = None
    band_name: Optional[str] = None
    max_area: Optional[int] = None
    year: Optional[int] = None


class GADM(Collection):
    gadm_id: str


@router.post('/gadm')
async def query_by_gadm_region(collection: GADM):
    return run(collection.dict())


class Coordinates(Collection):
    latitude: float
    longitude: float


@router.post('/coordinates')
async def query_by_coordinates(collection: Coordinates):
    return run(collection.dict())


class Boundary(Collection):
    boundary: Any


@router.post('/boundary')
async def query_by_geojson(collection: Boundary):
    return run(collection.dict())
