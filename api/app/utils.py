import os
import json
import numpy as np
from enum import Enum
import shutil

DATA_FOLDER = os.getenv('DATA_FOLDER', 'data')
SOURCE_FILENAME = 'source.csv'


class DataState(str, Enum):
    original = 'original'
    recalculated = 'recalculated'


# fix error "Object of type int64 is not JSON serializable"
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def get_param(params: dict, key: str, default=None):
    value = params.get(key, default)
    return default if value is None else value


def get_required_param(params, key: str):
    if key not in params:
        raise KeyError(f"Missing required '{key}'")
    return params.get(key)


def read_file(filepath: str):
    if not os.path.exists(filepath):
        return None
    try:
        with open(filepath, 'r') as f:
            return f.read()
    except Exception as e:
        print('file not found', filepath, str(e))
        return None


def read_json(filepath: str):
    if not os.path.exists(filepath):
        return None
    try:
        with open(filepath, 'r') as f:
            return json.load(f)
    except Exception:
        print('file not found', filepath)
        return {}


def to_string(data: dict): return json.dumps(data, indent=2, ensure_ascii=False, cls=NpEncoder)


def get_modified_at(filepath: str):
    return int(os.path.getmtime(filepath) * 1000) if os.path.exists(filepath) else None


def node_type(node: dict): return node.get('@type', node.get('type'))


def node_id(node: dict): return node.get('@id', node.get('id'))


def node_type_folder(node: dict): return os.path.join(DATA_FOLDER, node_type(node))


def node_folder(node: dict): return os.path.join(node_type_folder(node), node_id(node))


def node_config_path(node: dict): return os.path.join(node_folder(node), 'config.json')


def set_node_config(node: dict, key: str, value: str):
    filepath = node_config_path(node)
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    config = read_json(filepath) or {}
    print('using config', config, key, value)
    with open(filepath, 'w') as f:
        f.write(json.dumps({**config, key: value}, indent=2))


def node_config(node: dict): return read_json(node_config_path(node))


def log_filename(node: dict): return os.path.join(node_folder(node), 'run.log')


def error_filename(node: dict): return os.path.join(node_folder(node), 'error.log')


def node_data_path(node: dict, dataState: DataState = DataState.original):
    return os.path.join(node_folder(node), f"{dataState.value}.jsonld")


def node_source_path(node: dict):
    return os.path.join(node_folder(node), SOURCE_FILENAME)


def _delete_node(node: dict):
    base_folder = node_folder(node)
    if os.path.exists(base_folder):
        shutil.rmtree(base_folder)
        return True
    return False


def delete_node(id: str):
    return any([
        _delete_node({'@type': 'Cycle', '@id': id}),
        _delete_node({'@type': 'ImpactAssessment', '@id': id}),
        _delete_node({'@type': 'Site', '@id': id})
    ])


def is_equal(a: dict, b: dict): return json.dumps(a) == json.dumps(b)


def safe_delete(path: str): return os.remove(path) if os.path.exists(path) else None
