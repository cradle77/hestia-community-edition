import os
import json
from functools import reduce
from hestia_earth.schema import SchemaType, TermTermType

from .utils import (
    DATA_FOLDER, DataState,
    get_modified_at, error_filename, read_json, node_config, node_data_path, node_source_path
)


TERM_TYPES = [e.value for e in TermTermType]
SCHEMA_TYPES_LOWER = [e.value.lower() for e in SchemaType]
PREFIX = 'Missing lookup'


def _unique(values: list): return list({json.dumps(v): v for v in values}.values())


def _parse_filename(filepath: str):
    filename = filepath.split('.')[0]
    ext = '.xlsx' if filename in TERM_TYPES else '.csv'
    return filepath.replace('.csv', ext)


def _parse_lookup(data: dict):
    return {
        'filename': _parse_filename(data[PREFIX]),
        'termId': data.get('termid'),
        'column': data.get('column'),
        'model': data.get('model'),
        'term': data.get('term'),
        'key': data.get('key')
    }


def _csv_value(value: str): return (value or '').replace('[', '').replace(']', '')


def _parse_message_value(parts):
    try:
        [key, value] = parts.split('=')
        val = _csv_value(value)
        return {key.strip(): val} if key and val else {}
    except ValueError:
        return {}


def _parse_message(message: str):
    try:
        data = json.loads(message)
        logger = data.get('logger', '')
        parts = data.get('message', '').split(',')
        return reduce(lambda prev, curr: {**prev, **_parse_message_value(curr)}, parts, {'logger': logger})
    except json.decoder.JSONDecodeError:
        return {}


def parse_missing_lookups(data: str):
    lines = list(filter(lambda log: PREFIX in log, data.split('\n'))) if data else []
    messages = list(filter(lambda v: len(v.keys()) > 1, map(_parse_message, lines)))
    return _unique(list(map(_parse_lookup, messages)))


def node_metadata(type, id):
    node = {'@type': type, '@id': id}
    source_path = node_source_path(node)
    return {
        'id': id,
        'createdOn': get_modified_at(node_data_path(node, DataState.original)),
        'recalculatedOn': get_modified_at(node_data_path(node, DataState.recalculated)),
        'error': read_json(error_filename(node)),
        'config': node_config(node),
        'sourcePath': source_path.replace(DATA_FOLDER, '') if os.path.exists(source_path) else None
    }
