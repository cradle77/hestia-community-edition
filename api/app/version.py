from fastapi import APIRouter
from hestia_earth.models.version import VERSION as MODELS_VERSION
from hestia_earth.orchestrator.version import VERSION as ORCHESTRATOR_VERSION
from hestia_earth.validation.version import VERSION as VALIDATION_VERSION

from .dockerhub import latest_version, is_prerelease

VERSION = '2.6.2'

router = APIRouter(
    prefix='/version',
    tags=['Version'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_version():
    latest = latest_version()
    return {
        'current': VERSION,
        'latest': latest,
        'isPrerelease': is_prerelease(VERSION),
        'isLatest': VERSION == latest,
        'models': MODELS_VERSION,
        'orchestrator': ORCHESTRATOR_VERSION,
        'validation': VALIDATION_VERSION
    }
