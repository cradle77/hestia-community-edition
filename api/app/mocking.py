import os
from hestia_earth.schema import SchemaType
from hestia_earth.models import utils

from .utils import DataState, read_json, node_data_path


def _load_calculated_node(node, type: SchemaType, data_state='recalculated'):
    original_filepath = node_data_path({'@type': type.value, '@id': node.get('@id')}, DataState.original)
    recalculated_filepath = node_data_path({'@type': type.value, '@id': node.get('@id')}, DataState.recalculated)
    return read_json(recalculated_filepath) if os.path.exists(recalculated_filepath) else read_json(original_filepath)


def enable_mock():
    utils._load_calculated_node = _load_calculated_node
