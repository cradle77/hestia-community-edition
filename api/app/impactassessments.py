import os
import traceback
from typing import Any, Dict, Optional
from hestia_earth.schema import NodeType
from fastapi import APIRouter, HTTPException, Header
from fastapi.responses import PlainTextResponse

from .utils import (
    DataState, node_type_folder, log_filename, read_json, read_file, node_data_path, delete_node
)
from .calculate_utils import calculate, log_error
from .node_utils import node_metadata, parse_missing_lookups
from .list_utils import SortBy, SortOrder, paginate, order_by, filter_by


router = APIRouter(
    prefix='/impactassessments',
    tags=['Impact Assessments'],
    responses={404: {'description': 'Not found'}},
)


def _node_from_id(id: str): return {'@type': NodeType.IMPACTASSESSMENT.value, '@id': id}


@router.get('')
async def list_impact_assessments(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.createdOn, sortOrder: SortOrder = SortOrder.Descending
):
    node_type = NodeType.IMPACTASSESSMENT.value
    base_folder = node_type_folder({'@type': node_type})
    ids = os.listdir(base_folder) if os.path.exists(base_folder) else []
    results = filter_by([node_metadata(node_type, id) for id in ids], 'id', search)
    count = len(results)
    results = paginate(order_by(results, sortBy, sortOrder), offset, limit)
    return {'results': results, 'count': count}


@router.post('')
async def create_impact_assessment(data: Dict[str, Any], config: Optional[str] = Header(None)):
    try:
        return calculate(data, config)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}')
async def get_impact_assessment(id: str, dataState: DataState = DataState.original):
    try:
        node = _node_from_id(id)
        return read_json(node_data_path(node, dataState))
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}/metadata')
async def get_impact_assessment_metadata(id: str):
    try:
        return node_metadata(NodeType.IMPACTASSESSMENT.value, id)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.delete('/{id}')
async def delete_impact_assessment(id: str):
    return {'success': delete_node(id)}


@router.get('/{id}/log', response_class=PlainTextResponse)
async def get_impact_assessment_log(id: str):
    try:
        return read_file(log_filename(_node_from_id(id)))
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}/log/lookups')
async def get_impact_assessment_log_missing_lookups(id: str):
    try:
        logs = read_file(log_filename(_node_from_id(id)))
        return parse_missing_lookups(logs)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.post('/{id}/pipeline')
async def recalculate_impact_assessment(id: str, config: Optional[str] = Header(None)):
    try:
        node = _node_from_id(id)
        data = read_json(node_data_path(node, DataState.original))
        return calculate(data, config)
    except Exception as e:
        stack = traceback.format_exc()
        log_error(data, str(e), stack)
        raise HTTPException(status_code=500, detail=stack)
