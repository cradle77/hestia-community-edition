from hestia_earth.validation import validate
from hestia_earth.validation.validators import shared


def validate_nodes(nodes: list):
    # make sure we do not try to download any files here
    shared.download_hestia = lambda *args: {}
    return validate(nodes)
