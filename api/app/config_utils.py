import os
import json

from .utils import DATA_FOLDER, get_modified_at, is_equal, read_json


DEFAULT_CONFIG = 'default'
BASE_FOLDER = os.path.join(DATA_FOLDER, 'config')
os.makedirs(BASE_FOLDER, exist_ok=True)

SETTINGS_PATH = os.path.join(BASE_FOLDER, 'settings.json')
DEFAULT_SETTINGS = {
    'defaultFormat': 'Poore & Nemecek'
}


def base_folder(id: str = DEFAULT_CONFIG):
    return 'config' if id == DEFAULT_CONFIG or id is None else os.path.join(BASE_FOLDER, id)


def config_path(type: str, id: str = DEFAULT_CONFIG): return os.path.join(base_folder(id), f"{type}.json")


def load_config(type: str, id: str = DEFAULT_CONFIG):
    with open(config_path(type, id)) as f:
        return json.load(f)


def load_config_with_default(type: str, id: str = DEFAULT_CONFIG):
    default_path = os.path.join(base_folder(), f"{type}.json")
    if os.path.exists(default_path):
        with open(default_path) as f:
            default = json.load(f)

        try:
            config = load_config(type, id)

            return {
                'type': type,
                'updatedOn': get_modified_at(config_path(type, id)),
                'hasOverride': not is_equal(default, config),
                'default': default,
                'config': config
            }
        except FileNotFoundError:
            return {}
    else:
        return {}


def load_settings(): return read_json(SETTINGS_PATH) or DEFAULT_SETTINGS


def update_settings(key: str, value):
    settings = load_settings()
    settings[key] = value
    with open(SETTINGS_PATH, 'w') as f:
        return f.write(json.dumps(settings, indent=2))
