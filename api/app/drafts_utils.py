import os

from .utils import DATA_FOLDER

DRAFT_FOLDER = 'drafts'
DRAFT_BASE_FOLDER = os.path.join(DATA_FOLDER, DRAFT_FOLDER)


def draft_folder(id: str):
    folder = os.path.join(DRAFT_BASE_FOLDER, id)
    os.makedirs(folder, exist_ok=True)
    return folder


def draft_filepath(id: str): return os.path.join(draft_folder(id), 'data.json')
