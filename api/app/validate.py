import traceback
from typing import Any, Dict, List
from pydantic import BaseModel
from fastapi import APIRouter, HTTPException

from .validate_utils import validate_nodes

router = APIRouter(
    prefix='/validate',
    tags=['Data Validation'],
    responses={404: {'description': 'Not found'}},
)


class ValidateData(BaseModel):
    nodes: List[Dict[str, Any]]


@router.post('')
async def validate(data: ValidateData):
    try:
        return validate_nodes(data.nodes)
    except Exception:
        stack = traceback.format_exc()
        print(stack)
        raise HTTPException(status_code=500, detail=stack)
