# migrate ImpactAssessment data saved under Cycle
import os
import shutil
import json
from hestia_earth.schema import NodeType
from hestia_earth.utils.tools import non_empty_list, flatten

from ..utils import DATA_FOLDER, DataState, read_json, node_config_path, node_config, safe_delete
from ..drafts_utils import DRAFT_BASE_FOLDER
from ..calculate_utils import _impact_from_cycle, _clean_node


def _isVersionKey(key: str): return key in ['addedVersion', 'updatedVersion']


def _isValidValue(value):
    return value and (isinstance(value, list) or value.get('addedVersion') or value.get('updatedVersion'))


def _find_models_version(data: dict):
    versions = []

    for key, value in data.items():
        if _isVersionKey(key) and _isValidValue(value):
            versions.extend(value)
        elif isinstance(value, dict):
            versions.extend(value.get('addedVersion') or value.get('updatedVersion') or [])
        elif isinstance(value, list):
            versions.extend([_find_models_version(v) for v in value if isinstance(v, dict)])

    versions = non_empty_list(flatten(versions))
    return versions[0] if len(versions) > 0 else None


def _write_data(filepath: str, node: dict):
    with open(filepath, 'w') as f:
        f.write(json.dumps(_clean_node(node), indent=2))


def _write_config(node: dict, config: dict = None):
    if config:
        with open(node_config_path(node), 'w') as f:
            f.write(json.dumps(config, indent=2))


def _move_file(src, dest):
    try:
        shutil.move(src, dest)
    except Exception:
        pass


def _updateImpactLinkedNodes(data: dict, id: str):
    cycle = read_json(os.path.join(DATA_FOLDER, NodeType.CYCLE.value, id, f"{DataState.original.value}.jsonld"))
    data['@id'] = id
    data['cycle'] = {'@type': NodeType.CYCLE.value, '@id': id}
    if 'site' in cycle:
        data['site'] = {'@type': NodeType.SITE.value, '@id': id}
    return data


def _migrateImpact(cycle_id: str, config: dict):
    src_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id)
    dest_folder = os.path.join(DATA_FOLDER, NodeType.IMPACTASSESSMENT.value, cycle_id)
    print('Migrating impact:', src_folder)
    os.makedirs(dest_folder, exist_ok=True)

    original_filepath = os.path.join(src_folder, 'impact.jsonld')
    if os.path.exists(original_filepath):
        data = read_json(original_filepath)
        data = _updateImpactLinkedNodes(data, cycle_id)
        _write_data(os.path.join(dest_folder, f"{DataState.original.value}.jsonld"), data)
        safe_delete(original_filepath)

    recalculated_filepath = os.path.join(src_folder, 'impact-recalculated.jsonld')
    if os.path.exists(recalculated_filepath):
        data = read_json(recalculated_filepath)
        data = _updateImpactLinkedNodes(data, cycle_id)
        _write_data(os.path.join(dest_folder, f"{DataState.recalculated.value}.jsonld"), data)
        _write_config({'@type': NodeType.IMPACTASSESSMENT.value, '@id': cycle_id}, config)
        safe_delete(recalculated_filepath)

    _move_file(
        os.path.join(src_folder, 'impact-run.log'),
        os.path.join(dest_folder, 'run.log')
    )
    _move_file(
        os.path.join(src_folder, 'source.csv'),
        os.path.join(dest_folder, 'source.csv')
    )


def _migrateSiteData(cycle_id: str, dataState: DataState, config: dict = None):
    try:
        filepath = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id, f"{dataState.value}.jsonld")
        data = read_json(filepath).get('site') if os.path.exists(filepath) else None
        if data:
            dest_folder = os.path.join(DATA_FOLDER, NodeType.SITE.value, cycle_id)
            os.makedirs(dest_folder, exist_ok=True)
            data['@id'] = cycle_id
            _write_data(os.path.join(dest_folder, f"{dataState.value}.jsonld"), data)
            _write_config({'@type': NodeType.SITE.value, '@id': cycle_id}, config)
    except Exception:
        pass


def _migrateSite(cycle_id: str, config: dict):
    src_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id)
    print('Migrating site:', src_folder)
    _migrateSiteData(cycle_id, DataState.original)
    _migrateSiteData(cycle_id, DataState.recalculated, config)


def _migrateCycleDataState(cycle_id: str, dataState: DataState):
    filepath = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id, f"{dataState.value}.jsonld")
    if os.path.exists(filepath):
        data = read_json(filepath)
        if 'site' in data:
            data['site'] = {'@type': NodeType.SITE.value, '@id': cycle_id}
        _write_data(filepath, data)


def _migrateCycleData(cycle_id: str, config: dict):
    _write_config({'@type': NodeType.CYCLE.value, '@id': cycle_id}, config)
    _migrateCycleDataState(cycle_id, DataState.original)
    _migrateCycleDataState(cycle_id, DataState.recalculated)


def _migrateCycle(cycle_id: str):
    base_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value)
    config = node_config({'@type': NodeType.CYCLE.value, '@id': cycle_id}) or {'config': 'default'}

    # find models version from recalculated file
    recalculated_filepath = os.path.join(base_folder, cycle_id, f"{DataState.recalculated.value}.jsonld")
    version = _find_models_version(read_json(recalculated_filepath)) if os.path.exists(recalculated_filepath) \
        else None
    if version:
        config['version'] = version

    print('Migrating config', config)

    # migrate to ImpactAssessment
    if os.path.exists(os.path.join(base_folder, cycle_id, 'impact.jsonld')):
        _migrateImpact(cycle_id, config)

    # migrate to Site
    if not os.path.exists(os.path.join(DATA_FOLDER, NodeType.SITE.value, cycle_id)):
        _migrateSite(cycle_id, config)

    # migrate Cycle
    _migrateCycleData(cycle_id, config)


def _migrateCycles():
    ids = os.listdir(os.path.join(DATA_FOLDER, NodeType.CYCLE.value))
    for cycle_id in ids:
        try:
            _migrateCycle(cycle_id)
        except Exception as e:
            print('Could not migrate Cycle with id', cycle_id, '- reason:', str(e))
            continue


def _migrateDraftData(draft_id: str):
    draft_filepath = os.path.join(DRAFT_BASE_FOLDER, draft_id, 'data.json')
    print('Migrating draft:', draft_filepath)
    nodes = read_json(draft_filepath).get('nodes', [])
    impacts = []
    for node in nodes:
        impacts.append(node if node.get('type') == NodeType.IMPACTASSESSMENT.value else _impact_from_cycle(node))

    with open(draft_filepath, 'w') as f:
        f.write(json.dumps({'nodes': impacts}, indent=2))


def _migrateDraft(draft_id: str):
    draft_filepath = os.path.join(DRAFT_BASE_FOLDER, draft_id, 'data.json')
    if os.path.exists(draft_filepath):
        data = read_json(draft_filepath)
        all_impacts = all([n.get('type') == NodeType.IMPACTASSESSMENT.value for n in data.get('nodes', [])])
        if not all_impacts:
            _migrateDraftData(draft_id)


def _migrateDrafs():
    ids = os.listdir(DRAFT_BASE_FOLDER)
    for draft_id in ids:
        try:
            _migrateDraft(draft_id)
        except Exception as e:
            print('Could not migrate Draft with id', draft_id, '- reason:', str(e))
            continue


def run():
    return [_migrateCycles(), _migrateDrafs()]
