from .migrate_cycle_impacts import run as migrate_cycle_impacts

MIGRATIONS = [
    migrate_cycle_impacts
]


def run():
    print('Running migrations, please wait...')
    return list(map(lambda func: func(), MIGRATIONS))
