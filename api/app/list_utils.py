from enum import Enum


class SortBy(str, Enum):
    id = 'id'
    createdOn = 'createdOn'
    recalculatedOn = 'recalculatedOn'


class SortOrder(str, Enum):
    Ascending = 'asc'
    Descending = 'desc'


def paginate(values: list, offset: int, limit: int): return values[offset:(offset+limit)]


def order_by(values: list, key: SortBy, order: SortOrder):
    values = sorted(values, key=lambda x: x.get(key) or ('' if key == SortBy.id else 0))
    return values if order == SortOrder.Ascending else list(reversed(values))


def filter_by(values: list, key: str, value: str):
    return list(filter(lambda v: value.lower() in v.get(key, '').lower(), values)) if value else values
