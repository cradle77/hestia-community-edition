import json
from typing import Any, Dict
from fastapi import APIRouter, Body

from .config_utils import SETTINGS_PATH, update_settings, load_settings

router = APIRouter(
    prefix='/settings',
    tags=['Settings for UI'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_all():
    return load_settings()


@router.put('')
async def update_all(data: Dict[str, Any]):
    settings = load_settings()
    with open(SETTINGS_PATH, 'w') as f:
        f.write(json.dumps({**settings, **data}, indent=2))
    return {'success': True}


@router.get('/{key}')
async def get_single(key: str):
    return load_settings()[key]


@router.put('/{key}')
async def update_single(key: str, value: Any = Body(...)):
    update_settings(key, value)
    return {'success': True}
