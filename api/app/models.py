import traceback
from fastapi import APIRouter, HTTPException

from hestia_earth.models.requirements import list_models, get_all, get_term_ids


router = APIRouter(
    prefix='/models',
    tags=['Engine Models'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_models(
    termType: str = None, productTermId: str = None, productTermType: str = None, tier: str = None, siteType: str = None
):
    return list_models(
        termType=termType, productTermId=productTermId, productTermType=productTermType, tier=tier, siteType=siteType
    )


@router.get('/requirements')
async def get_models_requirements(
    termType: str = None, productTermId: str = None, productTermType: str = None, tier: str = None, siteType: str = None
):
    return get_all(
        termType=termType, productTermId=productTermId, productTermType=productTermType, tier=tier, siteType=siteType
    )


@router.get('/term-ids')
async def get_models_term_ids(
    termType: str = None, productTermId: str = None, productTermType: str = None, tier: str = None, siteType: str = None
):
    try:
        return get_term_ids(
            termType=termType, productTermId=productTermId, productTermType=productTermType,
            tier=tier, siteType=siteType
        )
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)
