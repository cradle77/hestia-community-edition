import os
import json
import shutil
from typing import Any, Dict
from pydantic import BaseModel
from fastapi import APIRouter, File, Form

from .drafts_utils import DRAFT_BASE_FOLDER, draft_folder, draft_filepath
from .list_utils import SortBy, SortOrder, paginate, order_by, filter_by
from .utils import DATA_FOLDER, SOURCE_FILENAME, get_modified_at, node_folder, read_json

router = APIRouter(
    prefix='/drafts',
    tags=['Drafts'],
    responses={404: {'description': 'Not found'}},
)


def _result(id: str):
    return {
        'id': id,
        'createdOn': get_modified_at(draft_filepath(id))
    }


@router.get('')
async def list_drafts(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.createdOn, sortOrder: SortOrder = SortOrder.Descending
):
    ids = os.listdir(DRAFT_BASE_FOLDER) if os.path.exists(DRAFT_BASE_FOLDER) else []
    results = filter_by(list(map(_result, ids)), 'id', search)
    count = len(results)
    results = paginate(order_by(results, sortBy, sortOrder), offset, limit)
    return {'results': results, 'count': count}


@router.get('/{id}')
async def get_draft(id: str):
    filepath = draft_filepath(id)
    nodes = read_json(filepath).get('nodes', [])
    source_path = os.path.join(draft_folder(id), SOURCE_FILENAME)
    return {
        'id': id,
        'createdOn': get_modified_at(filepath),
        'nodes': nodes,
        'sourcePath': source_path.replace(DATA_FOLDER, '') if os.path.exists(source_path) else None
    }


@router.post('')
async def create_draft(file: bytes = File(...), id: str = Form(...)):
    file_location = os.path.join(draft_folder(id), SOURCE_FILENAME)
    with open(file_location, "wb+") as f:
        f.write(file)


@router.put('/{id}')
async def update_draft(id: str, data: Dict[str, Any]):
    filepath = draft_filepath(id)
    with open(filepath, 'w') as f:
        f.write(json.dumps(data, indent=2))
    return {'success': True}


class DraftSubmit(BaseModel):
    type: str
    id: str


@router.post('/{id}')
async def submit_draft(id: str, data: DraftSubmit):
    src_file = os.path.join(draft_folder(id), SOURCE_FILENAME)
    if os.path.exists(src_file):
        shutil.copyfile(
            src_file,
            os.path.join(node_folder({'@type': data.type, '@id': data.id}), SOURCE_FILENAME)
        )


@router.delete('/{id}')
async def delete_draft(id: str):
    base_folder = draft_folder(id)
    if os.path.exists(base_folder):
        shutil.rmtree(base_folder)
        return {'success': True}
    else:
        return {'success': False}
