import traceback
from typing import Any, Dict, Optional
from fastapi import APIRouter, HTTPException, Header

from .utils import node_config
from .calculate_utils import calculate, log_error
from .config_utils import DEFAULT_CONFIG, load_config

router = APIRouter(
    prefix='/recalculate',
    tags=['Calculation'],
    responses={404: {'description': 'Not found'}},
)


@router.post('')
async def recalculate(data: Dict[str, Any], config: Optional[str] = Header(None)):
    try:
        return calculate(data, config)
    except Exception as e:
        stack = traceback.format_exc()
        log_error(data, str(e), stack)
        raise HTTPException(status_code=500, detail=stack)


@router.get('/config')
async def config(type: str, id: Optional[str]):
    try:
        # id of the Node, used to get the config
        config = node_config({'@type': type, '@id': id}).get('config', DEFAULT_CONFIG)
        return load_config(type, config)
    except Exception:
        return load_config(type)
