import os
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from .version import VERSION, router as version
from .migrations import run as run_migrations
from .mocking import enable_mock

from .calculate import router as calculate
from .config import router as config
from .cycles import router as cycles
from .download import router as download
from .drafts import router as drafts
from .ee import router as ee
from .impactassessments import router as impactassessments
from .models import router as models
from .search import router as search
from .settings import router as settings
from .sites import router as sites
from .terms import router as terms
from .upload import router as upload
from .validate import router as validate

API_PATH = os.getenv('API_PATH', '/')  # the API path without proxy
BASE_URL = os.getenv('BASE_URL', '/')  # the API path seen by client-side
VERSION_HEADER = 'X-API-VERSION'
PANDAS_VERSION_HEADER = 'X-PANDAS-VERSION'

api = FastAPI(root_path=BASE_URL)
api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=[VERSION_HEADER, PANDAS_VERSION_HEADER]
)


@api.middleware('http')
async def add_version_header(request: Request, call_next):
    response = await call_next(request)
    response.headers[VERSION_HEADER] = VERSION
    return response


@api.middleware('http')
async def add_pandas_header(request: Request, call_next):
    response = await call_next(request)
    try:
        import pandas as pd
        response.headers[PANDAS_VERSION_HEADER] = pd.__version__
    except ImportError:
        pass
    return response


api.include_router(calculate)
api.include_router(config)
api.include_router(cycles)
api.include_router(download)
api.include_router(drafts)
api.include_router(ee)
api.include_router(impactassessments)
api.include_router(models)
api.include_router(search)
api.include_router(settings)
api.include_router(sites)
api.include_router(terms)
api.include_router(upload)
api.include_router(validate)
api.include_router(version)


# init earth engine if present
if os.getenv('EARTH_ENGINE_KEY_FILE'):
    from hestia_earth.earth_engine import init_gee
    try:
        init_gee()
    except Exception as e:
        print("failed to initialise GEE, reason:", str(e))


run_migrations()
enable_mock()


if API_PATH != '/':
    app = FastAPI(root_path=BASE_URL)
    app.mount(API_PATH, api)
else:
    app = api
