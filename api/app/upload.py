import os
from fastapi import APIRouter, HTTPException, File, Form

from .calculate_utils import Type
from .utils import DATA_FOLDER, SOURCE_FILENAME

router = APIRouter(
    prefix='/upload',
    tags=['File upload'],
    responses={404: {'description': 'Not found'}},
)


@router.post('')
async def upload_file(file: bytes = File(...), type: Type = Form(...), id: str = Form(...)):
    file_location = os.path.join(DATA_FOLDER, type, id, SOURCE_FILENAME)
    print('saving file to', file_location)
    with open(file_location, "wb+") as f:
        f.write(file)


@router.post('/excel')
async def convert_excel_to_csv(file: bytes = File(...)):
    try:
        import pandas as pd
    except ImportError:
        raise HTTPException(status_code=500, detail="Use the 'ce-api' image version instead to convert Excel files.")

    return pd.read_excel(file, index_col=None).to_csv(None, index=None)
