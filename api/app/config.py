from functools import reduce
import os
import json
import shutil
from typing import Any, Dict
from fastapi import APIRouter

from .utils import get_modified_at
from .list_utils import SortBy, SortOrder, filter_by, order_by, paginate
from .config_utils import BASE_FOLDER, DEFAULT_CONFIG, config_path, load_config_with_default, base_folder
from .calculate_utils import Type

router = APIRouter(
    prefix='/config',
    tags=['Config'],
    responses={404: {'description': 'Not found'}},
)


def list_files(folder: str): return [os.path.join(folder, f) for f in os.listdir(folder) if not os.path.isdir(f)]


def last_modified_at(files: str):
    return sorted([get_modified_at(f) for f in files])[0]


def list_folders(folder: str):
    os.makedirs(folder, exist_ok=True)
    folders = [f for f in os.listdir(folder) if os.path.isdir(os.path.join(folder, f))]
    return [
        {
            'id': f,
            'createdOn': last_modified_at(list_files(os.path.join(folder, f)))
        } for f in folders
    ]


@router.get('')
async def list_configs(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.id, sortOrder: SortOrder = SortOrder.Descending
):
    results = [{'id': DEFAULT_CONFIG, 'createdOn': ''}] + list_folders(BASE_FOLDER)
    results = filter_by(results, 'id', search)
    count = len(results)
    results = paginate(order_by(results, sortBy, sortOrder), offset, limit)
    return {'results': results, 'count': count}


@router.get('/{id}')
async def get_configs(id: str):
    def load_config(prev: dict, type: Type):
        config = load_config_with_default(type.value, id)
        return {**prev, type.value: config} if config else prev

    return reduce(load_config, Type, {})


@router.get('/{id}/{type}')
async def get_config(id: str, type: Type):
    return load_config_with_default(type, id)


@router.post('/{id}')
async def create_config(id: str):
    # cannot create default config
    if id == DEFAULT_CONFIG:
        return {'success': False}

    os.makedirs(base_folder(id), exist_ok=True)
    for type in Type:
        src_file = os.path.join(base_folder(), f"{type}.json")
        if os.path.exists(src_file):
            shutil.copyfile(
                src_file,
                os.path.join(base_folder(id), f"{type}.json")
            )
    return {'success': True}


@router.put('/{id}/{type}')
async def update_config(id: str, type: Type, data: Dict[str, Any]):
    # cannot edit default config
    if id == DEFAULT_CONFIG:
        return {'success': False}

    with open(config_path(type, id), 'w') as f:
        f.write(json.dumps(data, indent=2))
    return {'success': True}


@router.delete('/{id}')
async def delete_config(id: str):
    # cannot delete default config
    if id == DEFAULT_CONFIG:
        return {'success': False}

    folder = base_folder(id)
    if os.path.exists(folder):
        shutil.rmtree(folder)
        return {'success': True}
    else:
        return {'success': False}
