import os
import traceback
from typing import Optional
from fastapi import APIRouter, HTTPException
from fastapi.responses import FileResponse

from .utils import DATA_FOLDER

router = APIRouter(
    prefix='/download',
    tags=['File download'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def upload_file(path: str, filename: Optional[str] = None):
    try:
        filepath = DATA_FOLDER + path
        filename = filename or os.path.basename(path)
        return FileResponse(path=filepath, media_type='application/octet-stream', filename=filename)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)
