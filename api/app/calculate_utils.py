import os
import json
import traceback
from enum import Enum
from hestia_earth.models.mocking import enable_mock
from hestia_earth.models.version import VERSION
from hestia_earth.schema import NodeType
from hestia_earth.utils.model import find_primary_product
from hestia_earth.orchestrator import run

from .utils import (
    DataState, error_filename, log_filename, node_folder, node_data_path, node_type, node_id, to_string,
    set_node_config, read_json, safe_delete
)
from .config_utils import load_config, DEFAULT_CONFIG


class Type(str, Enum):
    Cycle = NodeType.CYCLE.value
    ImpactAssessment = NodeType.IMPACTASSESSMENT.value
    Site = NodeType.SITE.value


def log_error(node: dict, error: str, stack: str):
    data = to_string({
        'error': error,
        'stack': stack
    })
    with open(error_filename(node), 'w') as f:
        f.write(data)


def _setup_logger(logger, log_to_file, log_filename: str):
    # make sure we don't add logs indefinitely
    for handler in logger.handlers:
        logger.removeHandler(handler)
    log_to_file(log_filename)


def create_log_file(node: dict, filename: str = None):
    try:
        filename = filename if filename else log_filename(node)
        # delete file first if exists
        safe_delete(filename)
        # set orchestrator and models so both log to the same file
        from hestia_earth.orchestrator.log import logger, log_to_file
        _setup_logger(logger, log_to_file, filename)
        from hestia_earth.models.log import logger as models_logger, log_to_file as models_log_to_file
        _setup_logger(models_logger, models_log_to_file, filename)
        return True
    except Exception as e:
        print('error creating log file', str(e))
        return False


def _save_data(node: dict, dataState: DataState = DataState.original):
    filepath = node_data_path(node, dataState)
    # print('Saving', dataState.value, node_type(node), node_id(node))
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    with open(filepath, 'w') as f:
        f.write(json.dumps(node, indent=2))


def _linked_node(node): return {'@type': node_type(node), '@id': node_id(node)}


def _clean_node(node: dict):
    # make sure we use @type/@id notation when nodes are saved in their own folder
    node = {'@type': node_type(node), '@id': node_id(node), **node}
    if 'type' in node:
        del node['type']
    if 'id' in node:
        del node['id']
    return node


def _calculate_node(data: dict, config: str = None):
    data = _clean_node(data)

    set_node_config(data, 'config', config or DEFAULT_CONFIG)
    set_node_config(data, 'version', VERSION)

    _save_data(data)

    create_log_file(data)
    # delete error file (if any)
    safe_delete(error_filename(data))

    enable_mock(data)

    print('Calculating', node_type(data), node_id(data))
    try:
        result = run(data, load_config(node_type(data), config))
    except Exception as e:
        stack = traceback.format_exc()
        log_error(data, str(e), stack)
        print('Error when calculating', stack)
        raise e

    _save_data(result, DataState.recalculated)

    return result


def _calculate_linked_node(node: dict, key: str, config: str = None):
    data = node.get(key)
    if data:
        data['@id'] = node_id(node)
        data = _calculate_node(data, config)
        # replace content with linked node
        node[key] = _linked_node(data)
        if key == 'site' and 'cycle' in node:
            node['cycle'][key] = _linked_node(data)


def _calculate_by_impact_assessment(data: dict, config: str = None):
    # 1. Calculate the Site
    _calculate_linked_node(data, 'site', config)

    # 2. Calculate the Cycle
    _calculate_linked_node(data, 'cycle', config)

    return _calculate_node(data)


def _impact_from_cycle(data: dict):
    cycle = {**data}
    product = find_primary_product(cycle) or {}
    return {
        '@type': NodeType.IMPACTASSESSMENT.value,
        '@id': node_id(data),
        'cycle': _linked_node(cycle),
        'site': _linked_node(cycle.get('site')),
        'product': product.get('term'),
        'endDate': cycle.get('endDate'),
        'country': cycle.get('site', {}).get('country'),
        'functionalUnitQuantity': 1,
        'allocationMethod': 'none',
        'dataPrivate': True
    }


def _calculate_by_cycle(data: dict, config: str = None):
    # fint the ImpactAssessment with the same id and recalculate. If not found, create a new one
    base_folder = node_folder({'@type': NodeType.IMPACTASSESSMENT.value, '@id': node_id(data)})
    original_impact_path = os.path.join(base_folder, 'original.jsonld')
    return _calculate_by_impact_assessment(
        read_json(original_impact_path) if os.path.exists(original_impact_path) else _impact_from_cycle(data),
        config
    )


RECALCULATE_BY_TYPE = {
    NodeType.CYCLE.value: _calculate_by_cycle,
    NodeType.IMPACTASSESSMENT.value: _calculate_by_impact_assessment
}


def _unknown_type(ntype: str):
    raise Exception(f"Unhandled type: {ntype}")


def calculate(data: dict, config: str = None):
    ntype = node_type(data)
    return RECALCULATE_BY_TYPE.get(ntype, lambda *args: _unknown_type(ntype))(data, config)
