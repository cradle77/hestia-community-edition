import os


os.environ.setdefault('LOG_LEVEL', 'DEBUG')
os.environ.setdefault('DOWNLOAD_FOLDER', '/app')
os.environ.setdefault('DOWNLOAD_FOLDER_GLOSSARY', '/app')
# os.environ.setdefault('DISTRIBUTION_DATA_FOLDER', '/app/distribution')
# disabled as distribution files need to be reviewed
os.environ.setdefault('VALIDATE_DISTRIBUTION', 'false')
