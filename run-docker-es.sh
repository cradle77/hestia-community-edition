#!/bin/sh

set -e

docker build --progress=plain \
  -t hestia-community-edition:es-latest \
  -f es/Dockerfile \
  .

docker run --rm \
  --name hestia-community-edition-es \
  -p 9200:9200 \
  hestia-community-edition:es-latest
