#!/bin/sh

set -e

docker build --progress=plain \
  -t hestia-community-edition:latest \
  -f api/Dockerfile \
  .

docker run --rm \
  --name hestia-community-edition \
  --env-file .env \
  --env DATA_FOLDER=/app/data \
  --env EARTH_ENGINE_KEY_FILE=/app/ee/credentials.json \
  --env ECOINVENT_V3_FILEPATH=/app/data/ecoinventV3_excerpt.csv \
  --env DISTRIBUTION_DATA_FOLDER=/app/distribution \
  -v ${PWD}/data:/app/data \
  -v ${PWD}/ee:/app/ee \
  -v ${PWD}/api/app:/app/app \
  -p 3000:80 \
  hestia-community-edition:latest uvicorn "app.main:app" --reload --host "0.0.0.0" --port "80"
