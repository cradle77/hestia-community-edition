# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.6.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.6.1...v2.6.2) (2023-03-20)


### Bug Fixes

* **ui:** update poore-nemecek converter to `0.4.3` ([8dbdb1e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8dbdb1efa9bf68a4f41d135053544792258118c2))

### [2.6.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.6.0...v2.6.1) (2023-03-17)


### Bug Fixes

* **requirements:** update orchestrator to `0.5.1` ([6c2f828](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6c2f8284ca5f9a8f1f5daace481b3bf044074be8))

## [2.6.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.5.0...v2.6.0) (2023-03-15)


### Features

* **requirements:** update `validation` to `0.17.0` ([0e62d9a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e62d9a2ee2f1d7801829d4cf16be521f915546b))
* **requirements:** update data-validation to `0.19.0` ([e7bf764](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e7bf764c9e40709b66876f1cfa09c1bcd9da6547))
* **requirements:** update models to `0.43.0` ([f814f13](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f814f13f2068901588fb2b21d4367e04aefb2aaf))
* **requirements:** update orchestrator to `0.5.0` ([7c52c81](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7c52c811ffe6738a9ca923ecdd37463902c02fac))
* **ui:** show all pagination pages ([ec52f34](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ec52f34339c62f8ddad332dd2d0a1bbf9aef7982))


### Bug Fixes

* **results details:** fix overflow on missing lookups ([a0e3907](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a0e3907da335a9908953196fb16fe0107bd1b694)), closes [#54](https://gitlab.com/hestia-earth/hestia-community-edition/issues/54)

## [2.5.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.4.0...v2.5.0) (2023-02-28)


### Features

* **requirements:** update models to `0.42.0` ([12f7448](https://gitlab.com/hestia-earth/hestia-community-edition/commit/12f7448429d03cbf147c5647f9f6cff7fe84e154))
* **results upload:** show formatted error and instructions on `Hestia` format ([2d4d1c1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d4d1c115f858b30d0f45e6e292c003aa18d8099)), closes [#45](https://gitlab.com/hestia-earth/hestia-community-edition/issues/45)
* **results upload:** show instructions link for `Hestia` format ([68f03ec](https://gitlab.com/hestia-earth/hestia-community-edition/commit/68f03ec3b3d6779ea17c453557d50aef580e182b))


### Bug Fixes

* **api:** disable distribution validation ([24c93e3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/24c93e3d220d04637a63c54bab215a62f161fd61))
* **results upload:** replace terms by name from `Hestia` format ([f17caab](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f17caab9ea3c1807941e00b141a0f115fe66039a)), closes [#45](https://gitlab.com/hestia-earth/hestia-community-edition/issues/45)
* **ui:** display correct UI version in settings ([b32d614](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b32d6143077a85cfe3a2cd4c370d74ff03047596))

## [2.4.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.3.0...v2.4.0) (2023-02-14)


### Features

* **requirements:** update models to `0.41.2` and validation to `0.16.1` ([dcad984](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dcad984d486a8a98669c3dec540a655bad8629f8))


### Bug Fixes

* **api calculate:** fix get Node configuration ([e7a4442](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e7a4442a0c2605bcb8dd93f8509ea21c5e438264))
* **results:** update unit for ecosystems in download CSV ([2c78d3e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2c78d3e6e62dbfe0932db1eecaa79101878f6a98))

## [2.3.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.2.1...v2.3.0) (2023-01-31)


### Features

* **requirements:** update models to `0.41.0` and validation to `0.16.0` ([adab9ee](https://gitlab.com/hestia-earth/hestia-community-edition/commit/adab9ee90b45be6154a4a9a2b752df17f9c8b2ab))
* **settings:** show ui version ([14303f6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/14303f6721da8f4f916499116df85568b136ab70))
* **ui recalculate:** handle error when recalculating ([4eaa5b2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4eaa5b2805d9abb1f48bca42d4fb7c4e5f0aef4a))
* **ui:** use schema `15` ([8416c07](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8416c07634461ebbf448157046b19df78aba216b))


### Bug Fixes

* **calculate:** return local config in `GET /config` endpoint ([b98b8b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b98b8b1aca61a6194d7e63ae80f70b6e5dd892cf)), closes [#40](https://gitlab.com/hestia-earth/hestia-community-edition/issues/40)
* **results details:** use new terms for biodiversity loss ([d6c1ef7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d6c1ef7474216c9756db98607baa39523821834f))

### [2.2.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.2.0...v2.2.1) (2023-01-17)


### Bug Fixes

* **results upload:** fix error setting to private ([355d9ac](https://gitlab.com/hestia-earth/hestia-community-edition/commit/355d9ac11cc299a52af3141be7b887985809f7ce))

## [2.2.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.1.0...v2.2.0) (2023-01-17)


### Features

* add proxy to get orchestrator configuration from gitlab ([37bd724](https://gitlab.com/hestia-earth/hestia-community-edition/commit/37bd724dab89e0ce66f2e266450807b5b9f97b86))
* **requirements:** update models to `0.40.1` and orchestrator to `0.4.5` ([e817239](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e817239c0016e0b0f1a2031f4c2f2da301c65b66))
* **requirements:** update validation to `0.14.3` ([aecba31](https://gitlab.com/hestia-earth/hestia-community-edition/commit/aecba3112cada1e6e362a6c31284ce9439cb9c3d))
* **requirements:** update validation to `0.15.0` ([b98105d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b98105d41de84d6b1f5b59d23f388650ec20853d))
* **results upload:** set `dataPrivate=true` by default ([f86a037](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f86a037eeb8f8eb3aa58d45ef347acb66a8fe7fe))
* **ui:** use schema `14.4.0` ([9055899](https://gitlab.com/hestia-earth/hestia-community-edition/commit/905589949d2e8ffcf54942b7012a8897a97b13ab))


### Bug Fixes

* **ui:** add missing dependencies for color generation ([29b3069](https://gitlab.com/hestia-earth/hestia-community-edition/commit/29b30693cb6d2d9fcf5fab5cf428116bf4f7302a))

## [2.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.0.0...v2.1.0) (2022-12-13)


### Features

* **requirements:** update data validation to `0.14.0` ([1075473](https://gitlab.com/hestia-earth/hestia-community-edition/commit/10754734b2c7a3d3b0a5eef45eb8c2ea35f728fd))
* **requirements:** update models to `0.39.1` and orchestrator to `0.4.1` ([2acc5e9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2acc5e9b9a272d261e80a52a5b19d2a7dea7b731))
* **requirements:** update models to `0.39` and orchestrator to `0.4` ([20e6407](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20e6407650f9a100c51525c908bd98091e90dc9b))
* **ui:** update schema to `14.2.0` and ui-components to `0.10.2` ([bd23db4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bd23db424c0520142d80bf6ee32df3375ab15420))

## [2.0.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.27.0...v2.0.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **requirements:** `ecoinventV3` model requires a valid license.
Please read the instructions in the README if you want to use the model.

### Features

* **requirements:** update models to `0.37.0` ([97e1f01](https://gitlab.com/hestia-earth/hestia-community-edition/commit/97e1f01b252e7b1f6fa2d018082a1a8b83de03f6))
* **requirements:** update models to `0.38.0` and orchestrator to `0.3.7` ([6ecd847](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6ecd8475f8d267c30068ba6c632ba10f646e831b))
* **ui:** add Driver Chart ([80c9303](https://gitlab.com/hestia-earth/hestia-community-edition/commit/80c93036eced1fdba0999b50627cb3bc583f281d))
* **ui:** update to ui-framework version 3 ([06ae381](https://gitlab.com/hestia-earth/hestia-community-edition/commit/06ae3817eb41d3d9d1a1fc82028d93a5ad98f02c))

## [1.27.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.26.0...v1.27.0) (2022-11-15)


### Features

* **requirements:** update models to `0.36.2` ([73eedf8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/73eedf87f666b6ad93130cc99b342c3ebb6c2c6b))
* **requirements:** update validation to `0.13.0` ([65a1f73](https://gitlab.com/hestia-earth/hestia-community-edition/commit/65a1f731cc1c93819f42fb63f2791e7b7c113b42))

## [1.26.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.25.0...v1.26.0) (2022-11-08)


### Features

* **requirements:** use models version `0.36.0` and orchestrator version `0.3.6` ([d0b878f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d0b878fcb36bcc8f7d6122781de756734002d442))
* **results details:** invert Inputs and Products ([9e2d8c7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9e2d8c728adb0755784a3475b38161a87d4ff1bd))


### Bug Fixes

* **navbar:** restrict to container size as on the main Hestia website ([75d8050](https://gitlab.com/hestia-earth/hestia-community-edition/commit/75d80502644c2a7304da91e4e83ef8257a9fae22))

## [1.25.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.24.0...v1.25.0) (2022-10-18)


### Features

* **api cycles:** create Cycle using JSON ([b839540](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b8395403b10cbccace1cc64f586aae9de9911986))
* **requirements:** update models to `0.35.0` and validation to `0.12.0` ([cd2cc3c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/cd2cc3c901da5823f4605406539d62ffa3ffe5a5))
* **requirements:** use models `0.35.1` ([b30496d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b30496d4138c8edf3eb21672fc6161f096f2d5e4))

## [1.24.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.23.0...v1.24.0) (2022-10-04)


### Features

* **requirements:** use models `0.34.1` ([439ed3d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/439ed3d458e7f1473db34e347cf8d30ad2a8311f))
* **requirements:** use validation `0.11.9` ([5c6760b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5c6760b79033616a1c8a2d2088b4f72c8273b68b))

## [1.23.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.22.0...v1.23.0) (2022-09-20)


### Features

* **requirements:** use models `0.34.0` and validation `0.11.4` ([a30a199](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a30a199790b300c8bbf32fa85bcd581705083746))
* **ui:** update schema to `11` and ui-components ([89b10ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/89b10cacba34a12971aa250da17b87cf55aac886))

## [1.22.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.21.0...v1.22.0) (2022-09-06)


### Features

* **requirements:** use models `0.33.0` ([247fa93](https://gitlab.com/hestia-earth/hestia-community-edition/commit/247fa9386bc9f73dcdf2ee8aa5647548b4e285f3))
* **requirements:** use validation `0.11.3` ([2fa3351](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2fa33513ba3ef4670fe4bd7edff6eea3aa0db305))

## [1.21.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.20.0...v1.21.0) (2022-08-23)


### Features

* **requirements:** use data-validation version `0.11.0` ([c6f77c7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c6f77c767af081c6c37fdb3da1517968aedb01d1))
* **requirements:** use models `0.32.2` and orchestrator `0.3.3` ([d7027e8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d7027e828a488d810a054e2e52d0a606040e8b3e))

## [1.20.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.19.1...v1.20.0) (2022-08-10)


### Features

* **es:** build pre-indexed version of ElasticSearch ([f89a754](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f89a754331e998ee1bd76aa3ab37f0159c677645))
* **ui:** update `schema` to `9.7.1` and `ui-components` to `0.3.3` ([90796e3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/90796e3aeb032cebc1761fa4ae55b0492578232e))

### [1.19.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.19.0...v1.19.1) (2022-07-26)


### Bug Fixes

* **ui:** fix errors impact data missing ([328ebbd](https://gitlab.com/hestia-earth/hestia-community-edition/commit/328ebbd8f43b8e7647129456cc90f12c62241c8a))

## [1.19.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.18.0...v1.19.0) (2022-07-26)


### Features

* **requirements:** use data-validation version `0.10.14` ([9c2424f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9c2424f54721f0454d02fc367144793b92765153))
* **requirements:** use models `0.30.2` and orchestrator `0.3.2` ([81bcfda](https://gitlab.com/hestia-earth/hestia-community-edition/commit/81bcfda8cfc5f512c3ea39a0e37c7446ea384e8b))
* **ui:** add page to accept terms of use before using ([772ef87](https://gitlab.com/hestia-earth/hestia-community-edition/commit/772ef87a1dd876241fb65ac579c2a248956749a0))


### Bug Fixes

* **api:** include all blank nodes logs ([282b060](https://gitlab.com/hestia-earth/hestia-community-edition/commit/282b060eb982735d9e00ff33beb1437b62665aa0))

## [1.18.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.17.0...v1.18.0) (2022-07-12)


### Features

* **api:** handle `run_required` in logs ([831e8ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/831e8ca7d9ebda40cc64fdb2c7cd8d11d5355bfa))
* **requirements:** use data-validation version `0.10.11` ([b04cf09](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b04cf09b7cc29edcae3e34b3e27e0e48c858c8e5))
* **requirements:** use models `0.28.0` and orchestrator `0.3.0` ([d2bd03f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d2bd03f35717f110d02a33a1926ec5468fd6f0fd))
* **requirements:** use models `0.29.0` and orchestrator `0.3.1` ([c586b86](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c586b8689923b30409473b83ba9ebe6439d2068d))
* **ui:** update schema to `9.0.0` ([5434c84](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5434c84707b634049f0cb9fd92da62127322443a))
* **ui:** update schema to `9.2.0` and ui-components to `0.2.4` ([4597ff7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4597ff780d820687273ebd055d0280ecc6dc7176))

## [1.17.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.16.0...v1.17.0) (2022-06-21)


### Features

* **ui:** update `ui-components` to `0.1.1` ([3142d64](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3142d647035d0db9171bbfbc0053ddcde2de0d61))

## [1.16.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.15.0...v1.16.0) (2022-06-07)


### Features

* **api:** add routes to get model requirements ([101b7e2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/101b7e225b48e949b75a0af44276b76dca4464c3))
* **node:** handle `Transformation` ([5be745b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5be745b41a947ea973e8c42c04c83a20cdd1579f))

## [1.15.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0...v1.15.0) (2022-05-24)


### Features

* **api:** use python `3.9` ([94da8f8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/94da8f887ff44ff136a240d17d770faaba27aa78))
* **requirements:** use data-validation version `0.10.6` ([89555f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/89555f131ad284297c814132418b4e907c99b6e6))
* **ui settings:** show configuration in better format ([70cf274](https://gitlab.com/hestia-earth/hestia-community-edition/commit/70cf274ac995eebd39339df7d36e031700dfff3e))

## [1.14.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-2...v1.14.0) (2022-04-23)


### Features

* **results new:** set default cycle as private ([b89965d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b89965dfb9dcc85fd55d774e2fafbf2a5f9652c6))


### Bug Fixes

* **ui:** handle pre-release in version data ([e0f0b18](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e0f0b18aaaa46cba5fe46a112bd11618140f618c))

## [1.14.0-2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-1...v1.14.0-2) (2022-04-17)


### Features

* **settings:** edit default format for upload ([a350d8f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a350d8ff939ad4591c1ea5ffa443abdcae480264))
* **version:** show notification when new version is available ([e094074](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e094074ff038e5ed263f2aa437cedcde0fd7bc52))

## [1.14.0-1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-0...v1.14.0-1) (2022-04-13)

## [1.14.0-0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.13.0...v1.14.0-0) (2022-04-13)


### Features

* **api:** handle using subpath ([55d9139](https://gitlab.com/hestia-earth/hestia-community-edition/commit/55d91396e0726a84766aaff84e870c2031338b51))
* **ui:** add navigation menu in results page ([2532a68](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2532a6851b885499d125fa0ea1cbd966eda54fd1))
* **ui:** show config used and auto-select on recalculate confirm ([a5c7e8c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a5c7e8c9955e3f87ae0ccf164a3fdc0d4bfa4d7e))
* **ui:** support upload of Excel files ([71f00c1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/71f00c1463239513d13608549e8d0bc6405f9b89))

## [1.13.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.12.0...v1.13.0) (2022-04-12)


### Features

* **requirements:** use data-validation version `0.10.5` ([ae221ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ae221cab93ec304b2953494b2329419fe9624d73))
* **requirements:** use models version `0.25.0` ([1031b19](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1031b195bc19c2ae6bd3e16ad126161207bfbadf))
* **requirements:** use orchestrator version `0.2.20` ([c900bfb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c900bfb24ab2d3022604bbafe1ab4a9e0a8ccd26))
* **requirements:** use validation version `0.10.4` ([b9e7f18](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b9e7f18f6f00ed154e67c5319c1caea54da49359))
* **results:** display cycles practices ([e883334](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e883334ad128805fdff85d65e69e40fd3d11cb29))
* **styles:** hide `Data Aggregated` notice site-wide ([6cca6e9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6cca6e99b87f3785430821c965c43597d54cdbb8))
* **ui results:** change `Emissions` title for `ImpactAssessment` ([0aa6395](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0aa6395aa01994fbcc8159b6d33713b53ba99899))

## [1.12.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.11.0...v1.12.0) (2022-03-29)


### Features

* **api config:** handle config not found ([6703a0d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6703a0d49a9ef4903c3514d33447565d04be6d1d))
* **api:** override api docs url with `BASE_URL` env variable ([66d7ceb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/66d7ceb1c71b68a897577ea30592fe83921c72c2))
* **api:** return `X-API-VERSION` header ([52bf372](https://gitlab.com/hestia-earth/hestia-community-edition/commit/52bf37297de7c6a4267a4a73f8b0e086739ef4a5))
* **config:** add ability to create multiple configurations ([d327b0b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d327b0bea046c3de0258d0a904673e5834405706))
* **requirements:** use models version `0.24.10` ([ce00d7d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ce00d7daa28ac4e91d2eb752896e1521a019006d))
* **requirements:** use validation version `0.9.3` ([8a84290](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8a84290d21d40649c33c69cd4c1066957cb37ecf))
* **ui results upload:** add link to P&N documentation ([0e18a8b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e18a8b4a49a4e768c15bff50a8501dc8609ca90))
* **ui results:** prevent validation when submitting ([5590688](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5590688b08a723eb95f02ab141d98769933ec1f7))
* **ui results:** rename submit button to calculate ([4fc1bd4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4fc1bd470e7becf9be2064c653fbffbdddaca03c))
* **ui results:** show error summary ([05733d2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/05733d2dcb5993e183c29835729dd2c52a832749))
* **ui:** display API version in settings ([76996dc](https://gitlab.com/hestia-earth/hestia-community-edition/commit/76996dc75c944fb906993311594a154644ac4ab4))
* **ui:** move code from hestia private frontend repo ([a63a0c8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a63a0c825572161dbef93921ae65bbb065936dcd))
* **ui:** validate schema on draft before calculating ([bc3f774](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bc3f774bc20abfd8c11536fb137bcb62860cdc76))


### Bug Fixes

* **api:** use `root_path` fix Swagger API not showing ([7830539](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7830539eeea67e4031d3624e0f10b1b18200bf6e))
* **config:** make sure `config` folder exists ([c452a67](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c452a6705a1a130a70391b4bf987c808da774b67))
* **cycles:** fix sorting error ([a45db9b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a45db9b5cc9709b01efc622188280ee09f2496ae))
* **drafts:** fix sorting error ([9ca577a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9ca577a7815d240e83247b382098272d4584dfda))
* **ui results:** remove delete node from list in Draft mode ([c0c858e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c0c858e05279ba6a2921e9181d7da290039937bf))
* **ui results:** show loader while validating the data ([2d20ac4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d20ac47572bab7d2ff4fe0fe4a5243e9458d46f))

## [1.11.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.9...v1.11.0) (2022-03-15)


### Features

* **requirements:** use models version `0.24.8` ([7d07972](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7d079722419a349dca7a4848c17361ef3efad8a7))

### [0.1.9](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.8...v0.1.9) (2022-03-11)


### Features

* **requirements:** use models version `0.24.7` ([9ccd7e0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9ccd7e0f73df1ff98259093e1da5b321d284a7bf))


### Bug Fixes

* **drafts:** handle draft without source on submit ([033f758](https://gitlab.com/hestia-earth/hestia-community-edition/commit/033f758b963593ad4b8f6c5c83ba412ac92f1e68))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.7...v0.1.8) (2022-03-10)


### Features

* **requirements:** use models version `0.24.6` ([fbfa824](https://gitlab.com/hestia-earth/hestia-community-edition/commit/fbfa824978ac1e51a9363cd11130217e21d10e27))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.6...v0.1.7) (2022-03-09)


### Bug Fixes

* **calculate:** delete log file before re-running calculations ([234bab0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/234bab0f691a19de7451164103283aa287494103))
* **calculate:** remove duplicated logs in impact-run.log ([54f2d7a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/54f2d7ab812858cb68797dd3565a756635484cef))
* **validate:** validate a list of nodes ([63549f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/63549f1e306a4574e252ef50465a6efafb5b0966))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.5...v0.1.6) (2022-03-08)


### Features

* **requirements:** use models version `0.24.5` ([3e1ff2a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3e1ff2a9a5a6e2d5a41e922d7dc1893d7e17b0c0))
* **validate:** add endpoint to validate a node ([9443f41](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9443f41b22401798bd12ad67db7171ec0318e744))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.4...v0.1.5) (2022-03-07)


### Bug Fixes

* **ee:** fix run no type argument ([d0ecf3b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d0ecf3b48820561a7367f29d886d24b0c9d4a6ce))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.3...v0.1.4) (2022-03-07)


### Bug Fixes

* **api:** change log level for saving calculation logs ([be14c1b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/be14c1b9625de59e6fd189fa0d594b331d81e1d6))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.2...v0.1.3) (2022-03-07)


### Features

* **calculate:** save errors to file ([bcd6b3a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bcd6b3aad8b32792084b798e1b5bd61654d62fc4))
* **drafts:** create, update and list drafts ([be18605](https://gitlab.com/hestia-earth/hestia-community-edition/commit/be186057c4099f3be79c406bb04ffa00e56950c0))
* handle read file errors return empty ([2d02d2a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d02d2ac39bfab3e5183b753805472f6afe0bdc2))
* **requirements:** use models version `0.24.4` ([2f1b0a3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2f1b0a3a2a8b6a54df19094926b5c93de4799a8a))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.1...v0.1.2) (2022-02-23)


### Features

* **requirements:** use models version `0.24.1` ([20a634c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20a634c16c1f8777d2dd47d7033e85e5b1b98174))
* **requirements:** use models version `0.24.3` ([61703aa](https://gitlab.com/hestia-earth/hestia-community-edition/commit/61703aad631f149de02733e14a272f8c2fc41b5e))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.0...v0.1.1) (2022-02-21)


### Features

* **config:** add routes to get/update/delete ([8ce8dad](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8ce8dadd316e911adce82865d1b9933b5d89fa10))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.0.1...v0.1.0) (2022-02-18)


### ⚠ BREAKING CHANGES

* **results:** `/results` prefix routes is now `/cycles`
* **calculate:** `POST /calculate` is now `POST /recalculate`

### Features

* **cycles:** add filter/pagination to list ([d2616ae](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d2616aee19de6a5af0c2825dc4d4b0b177a11f9a))
* **cycles:** add route get logs by model ([2dd4318](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2dd43187d3918d4512c5e4cc9ffd25de7e1a444e))
* **impact assessments:** add get impact and logs routes ([9fde559](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9fde5598e17fc49907bd7cdec0e768b2e74ff645))
* **search:** redirect search requests to Hestia API ([0e92ce0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e92ce0ec6fb5521822aaf3d5230efaeb465efa1))


### Bug Fixes

* **app:** only require `EARTH_ENGINE_KEY_FILE` env variable ([ce7989d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ce7989df89b0266cacf1c160ae90e2aa7f994f85))
* remove trailing slash in routes ([fa2222e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/fa2222e2e08222f02b9dd5973e438d2a3ac20b59))


* **calculate:** change prefix to `/recalculate` ([e0d34ac](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e0d34aca27d650e44e079479471e075d0fdd2ddc))
* **results:** moved to cycles ([1faabe4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1faabe4617f8963321d858218e4af78b65c99daa))

### 0.0.1 (2022-02-16)


### Features

* handle site calculations ([97543a8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/97543a8742c2c35b78c899afa440b338026bc79f))
* **ui:** add download summary for recalculated data ([20212a0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20212a020ca3b1e53877039faecd9e7217719f54))
* **ui:** add frontend ([c3cc3f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c3cc3f1426b7d66e9e35a836be34917c10a507bb))
* **ui:** add geospatial search by coordinates ([a743d4e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a743d4e791f53349247d7b603c4722db08d21c2f))


### Bug Fixes

* **ui:** make it clearer how to calculate data ([aaef3b4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/aaef3b49eb7cd4b3609a24922e21d015b22dbd72))
