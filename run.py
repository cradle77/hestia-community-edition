# this loads the `.env` file and export as environment variables
from dotenv import load_dotenv
load_dotenv()


import sys
import json
# this will mock requests to Hestia Search API
from hestia_earth.models.mocking import enable_mock
from hestia_earth.earth_engine import init_gee
from hestia_earth.orchestrator import run


def main(args):
    filepath = args[0]
    with open(filepath) as f:
        data = json.load(f)

    # load the Cycle.json or ImpactAssessment.json configuration file depending on the @type present in the data
    node_type = data['@type']
    with open(f"config/{node_type}.json") as f:
        config = json.load(f)

    enable_mock()
    init_gee()
    print(json.dumps(run(data, config)))


if __name__ == "__main__":
    main(sys.argv[1:])
