stages:
  - test
  - prepublish
  - publish

default:
  image: docker:latest
  before_script:
    - docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD" $DOCKER_REGISTRY

lint-commit:
  image: node:14
  stage: test
  before_script:
    - npm install -g npm@7
    - npm install
  script:
    - git fetch origin develop
    - npx commitlint --from=origin/develop
  only:
    - branches
  except:
    - develop
    - main

lint-api:
  image: python:3.9
  stage: test
  before_script:
    - pip install -r requirements-app.txt
    - pip install -r requirements-main.txt
    - pip install flake8
  script:
    - flake8
  except:
    - main
    - tags

build-api-docker-dev:
  stage: test
  services:
    - docker:dind
  before_script:
    - echo "skip before script"
  script:
    - docker build -f api/Dockerfile -t ce-api:dev --build-arg BRANCH=develop --build-arg API_URL=$API_URL_DEVELOP .
  only:
    - branches
  except:
    - main
    - develop

update-version:
  image: node:14-alpine
  stage: prepublish
  artifacts:
    paths:
      - api/app/version.py
      - ui/src/environments
      - ui/package.json
  before_script:
    - export SUFFIX="${CI_COMMIT_SHORT_SHA}"
    - export DATE="${CI_JOB_STARTED_AT}"
    - echo "using version suffix ${SUFFIX}"
  script:
    - node scripts/update-package-version.js "${SUFFIX}" "${DATE}"
  only:
    - develop

publish-api-docker-latest:
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -f api/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-api:latest" --build-arg BRANCH=develop --build-arg API_URL=$API_URL_DEVELOP .
    - docker push "$DOCKER_REGISTRY_BASE/ce-api:latest"
  only:
    - develop

# deploy version with aggregated models
publish-api-aggregated-docker-latest:
  stage: publish
  services:
    - docker:dind
  script:
    - sed -i -e 's|RUN python scripts/remove_aggregated_model.py||g' api/Dockerfile
    - docker build -f api/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-api-aggregated:latest" --build-arg BRANCH=develop --build-arg API_URL=$API_URL_DEVELOP .
    - docker push "$DOCKER_REGISTRY_BASE/ce-api-aggregated:latest"
  only:
    - develop

publish-api-docker:
  stage: publish
  services:
    - docker:dind
  script:
    - export TAG=${CI_COMMIT_REF_NAME#?}
    - docker build -f api/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-api:$TAG" .
    - docker push "$DOCKER_REGISTRY_BASE/ce-api:$TAG"
  only:
    - tags

# deploy version with aggregated models
publish-api-aggregated-docker:
  stage: publish
  services:
    - docker:dind
  script:
    - sed -i -e 's|RUN python scripts/remove_aggregated_model.py||g' api/Dockerfile
    - export TAG=${CI_COMMIT_REF_NAME#?}
    - docker build -f api/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-api-aggregated:$TAG" .
    - docker push "$DOCKER_REGISTRY_BASE/ce-api-aggregated:$TAG"
  only:
    - tags

lint-ui:
  image: node:14
  stage: test
  before_script:
    - cd ui
    - npm install -g npm@7
    - npm install
  script:
    - npm run lint
  except:
    - main
    - tags

build-ui:
  image: node:14
  stage: prepublish
  artifacts:
    paths:
      - ui/dist
  before_script:
    - cd ui
    - npm install -g npm@7
    - npm install
  script:
    - node update-api-url.js src/environments/environment.ts $API_URL
    - npm run build
  only:
    - branches
  except:
    - main

deploy-ui-dev:
  image: python:latest
  stage: publish
  environment:
    name: $CI_COMMIT_BRANCH
    on_stop: remove-ui-dev
  before_script:
    - pip install awscli
  script:
    - find ./ui/dist -type f -name "*.html" -print0 | xargs -0 sed -i -e "s|<base href=\"/\">|<base href=\"/$CI_COMMIT_BRANCH/\">|g"
    - aws s3 sync ./ui/dist s3://$BUCKET_DEV/${CI_COMMIT_BRANCH}/
    - aws cloudfront create-invalidation --distribution-id $DISTRIB_ID_DEV --paths '/*'
  only:
    - branches
  except:
    - develop
    - main

remove-ui-dev:
  image: python:latest
  stage: publish
  when: manual
  environment:
    name: $CI_COMMIT_BRANCH
    action: stop
  variables:
    GIT_STRATEGY: none
  before_script:
    - pip install awscli
  script:
    - aws s3 rm s3://$BUCKET_DEV/${CI_COMMIT_BRANCH} --recursive
  only:
    - branches
  except:
    - develop
    - main

deploy-ui-develop:
  image: python:latest
  stage: publish
  environment:
    name: develop
  before_script:
    - pip install awscli
  script:
    - find ./ui/dist -type f -name "*.html" -print0 | xargs -0 sed -i -e "s|window.process.env.API_URL = '/api';|window.process.env.API_URL = '${API_URL}';|g"
    - aws s3 sync ./ui/dist s3://$BUCKET_DEVELOP/
    - aws cloudfront create-invalidation --distribution-id $DISTRIB_ID_DEVELOP --paths '/*'
  only:
    - develop

publish-ui-docker-latest:
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -t "$DOCKER_REGISTRY_BASE/ce-ui:latest" ./ui
    - docker push "$DOCKER_REGISTRY_BASE/ce-ui:latest"
  only:
    - develop

publish-ui-docker:
  stage: publish
  services:
    - docker:dind
  script:
    - export TAG=${CI_COMMIT_REF_NAME#?}
    - docker build -t "$DOCKER_REGISTRY_BASE/ce-ui:$TAG" ./ui
    - docker push "$DOCKER_REGISTRY_BASE/ce-ui:$TAG"
  only:
    - tags

build-es-docker-dev:
  stage: test
  services:
    - docker:dind
  before_script:
    - echo "skip before script"
  script:
    - docker build -f es/Dockerfile -t ce-es:dev --build-arg API_URL=$API_URL_DEVELOP .
  only:
    - branches
  except:
    - develop
    - main

publish-es-docker-latest:
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -f es/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-es:latest" --build-arg API_URL=$API_URL_DEVELOP .
    - docker push "$DOCKER_REGISTRY_BASE/ce-es:latest"
  only:
    - develop

publish-es-docker:
  stage: publish
  services:
    - docker:dind
  script:
    - export TAG=${CI_COMMIT_REF_NAME#?}
    - docker build -f es/Dockerfile -t "$DOCKER_REGISTRY_BASE/ce-es:$TAG" .
    - docker push "$DOCKER_REGISTRY_BASE/ce-es:$TAG"
  only:
    - tags
