# Hestia Community Edition

> Community Edition of the Hestia Calculations

Provides a self-hosted installation of everything you need to run calculations on your server or computer.

## Getting started

### Prerequisites

You will need to install the following softwares:
1. Python `3` (we recommend using Python `3.6` minimum)
2. Make sure the following tools are installed:
    - [curl](https://www.tecmint.com/install-curl-in-linux/)
    - [unzip](https://www.tecmint.com/install-zip-and-unzip-in-linux/)
    - [jq](https://stedolan.github.io/jq/download/)

### Install

Run the `install.sh` script and choose your answer when prompted:
```bash
$ ./install.sh
```

**Note**: you will be prompted if you want to use the [Hestia Earth Engine library](https://gitlab.com/hestia-earth/hestia-earth-engine).
This is an additional tool that will allow you to gap-fill geographical information about your [Site](https://www.hestia.earth/schema/Site).
If you want to use it, please follow the [Getting Started instructions](https://gitlab.com/hestia-earth/hestia-earth-engine#getting-started) after installation is complete, and add the `credentials.json` file under `ee` folder.

### Using Docker and Docker Compose

1. Create a `.env` file with the following content:
```
# change this to set the maximum size of the site that can be gap-filled. Using a very high value will slow down calculations considerably or may even result in errors
MAX_AREA_SIZE=100000
# this variable is only needed when using the Hestia Earth Engine library
# points to the credentials.json file retrieve from Google Cloud, could be placed anywhere a mount point is available
EARTH_ENGINE_KEY_FILE=/app/ee/credentials.json
```
2. Run `docker-compose up --build`
3. Open a browser on http://localhost

Alternatively, you can also use the [Hestia's official images](https://hub.docker.com/u/hestiae) by running:
```bash
$ docker-compose -f docker-compose.prod.yml up
```

#### Stable Versions

There are 2 different versions of the CE API for Docker:

| Version | Run Aggregated model |
| ------ | ------ |
| [CE API](https://hub.docker.com/repository/docker/hestiae/ce-api) | ❌ |
| [CE API Aggregated](https://hub.docker.com/repository/docker/hestiae/ce-api-aggregated) | ✅ |

#### Using the latest tag

We provide [stable releases](https://gitlab.com/hestia-earth/hestia-community-edition/-/releases) on a regular basis. Both `hestiae/ce-api` and `hestiae/ce-ui` need to be used with the same tag.

If you want to use unstable releases that are published as soon as new features are added, you can pull the `latest` tag instead, but we can not guarantee everything will work smoothly. Thefore we do **not** recommend this in a production environment.

### Using the ecoinventV3 model

The `ecoinventV3` model requires a valid Ecoinvent license to run. If you have a license and want to use the model:
1. Send us an email at community@hestia.earth.
2. If your license is valid, we will provide you with a data file.
3. Set the `ECOINVENT_V3_FILEPATH` env variable to point to where you want to store that file.

## Contributing

If you wish to contribute, please read our [contributing guidelines](./CONTRIBUTING.md) first.
